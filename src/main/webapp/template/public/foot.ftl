<#import "/template/public/base.ftl" as my> 
<div class="layout-footer">
    <div class="footer">
      <div style="background-color:#383d61" class="footer--bg"></div>
      <div class="footer--inner">
        <div class="container">
          <div class="footer_main">
            <div class="am-g">
              <div class="am-u-md-3 ">
                <div class="footer_main--column">
                	<@myarticle cid=30 start=1 limit=1; map>
                  <strong class="footer_main--column_title"><@my.mapCategory map=map /></strong>
                  <div class="footer_about">
                  	  <div class="footer_about--text">
                  	  <#if map['articles']??>
                  	  <#list map['articles'] as a>
                      ${a.remark}
                      </#list>
                      </#if>
                      </div>
                    </div>
                    </@myarticle>
                </div>
              </div>

              <div class="am-u-md-3 ">
                <div class="footer_main--column">
                  <@myarticle cid=26 start=1 limit=5; map>
                  <strong class="footer_main--column_title"><@my.mapCategory map=map /></strong>
                  <ul class="footer_navigation">
                  	<#if map['articles']??>
                  	  <#list map['articles'] as a>
                    	<li class="footer_navigation--item"><a href="${my.base+map['category'].htmlUrl}/${a.id}.html" class="footer_navigation--link">
                    	<@my.showtitle title="${a.title}" len=14 />
                    	</a></li>
                      </#list>
                    </#if>
                  </ul>
                  </@myarticle>
                </div>
              </div>

              <div class="am-u-md-3 ">
                <div class="footer_main--column">
                  <@myarticle cid=29 start=1 limit=5; map>
                  <strong class="footer_main--column_title"><@my.mapCategory map=map /></strong>
                  <ul class="footer_navigation">
                    <#if map['articles']??>
                     <#list map['articles'] as a>
                    <li class="footer_navigation--item"><a href="${my.base+map['category'].htmlUrl}/${a.id}.html" class="footer_navigation--link">
                    <@my.showtitle title="${a.title}" len=12 /></a></li>
                     </#list>
                    </#if>
                  </ul>
                 </@myarticle>
                </div>
              </div>

              <div class="am-u-md-3 ">
                <div class="footer_main--column">
                  <strong class="footer_main--column_title">联系详情</strong>
                  <ul class="footer_contact_info">
                    <li class="footer_contact_info--item"><i class="am-icon-phone"></i><span>服务专线：${siteinfo.contactFixedPhone}</span></li>
                    <li class="footer_contact_info--item"><i class="am-icon-envelope-o"></i><span>test.com</span></li>
                    <li class="footer_contact_info--item"><i class="am-icon-map-marker"></i><span>${siteinfo.contactAddr}</span></li>
                    <li class="footer_contact_info--item"><i class="am-icon-clock-o"></i><span>周一到周五, 9am - 6 pm; </span></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>