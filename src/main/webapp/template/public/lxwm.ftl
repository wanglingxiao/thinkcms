<#assign base = request.contextPath+"/html"/>
<div class="section" style="margin-top:0px;background-image: url('${base}/assets/images/pattern-light.png');">
      <div class="container">
        <!--index-container start-->
        <div class="index-container">
          <div class="am-g">
            <div class="am-u-md-4">
              <div class="contact_card">
  							<i style="color:#59bcdb" class="contact_card--icon am-icon-phone"></i>
  							<strong class="contact_card--title">联系我们</strong>
  							<p class="contact_card--text">我们的电话 <br> <strong>+86 21 63142110</strong></p>
                <button type="button" class="am-btn am-btn-secondary">打电话给我们吧</button>
  						</div>
            </div>
            <div class="am-u-md-4">
              <div class="contact_card">
  							<i style="color:#59bcdb" class="contact_card--icon am-icon-envelope-o"></i>
  							<strong class="contact_card--title">我们的邮箱</strong>
  							<p class="contact_card--text">邮箱地址是 <br> <strong><a href="endurancew@hotmail.com">endurancew@hotmail.com</a>,</strong> <br> 收到邮件我们会尽快回复您</p>
                <button type="button" class="am-btn am-btn-secondary">写邮件给我们吧</button>
  						</div>
            </div>
            <div class="am-u-md-4">
              <div class="contact_card">
  							<i style="color:#59bcdb" class="contact_card--icon am-icon-map-marker"></i>
  							<strong class="contact_card--title">我们的地址</strong>
  							<p class="contact_card--text">具体地点在 <br> <strong>上海市浦东新区金湘路,</strong> <br> 225弄11号17楼1732室</p>
                <button type="button" class="am-btn am-btn-secondary">查看地图</button>
  						</div>
            </div>
          </div>
        </div>
        <!--index-container end-->
      </div>
    </div>