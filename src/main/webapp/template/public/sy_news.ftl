<#assign base = request.contextPath+"/html"/>
<div class="section">
      <div class="container">
        <div class="section--header">
					<h2 class="section--title">新闻中心</h2>
					<p class="section--description">
						全球领先HTML5企业移动化解决方案供应商，由前微软美国总部IE浏览器核心研发团队成员及移动互联网行业专家在美国西雅图创立
						<br>获得了微软创投的扶持以及晨兴资本、IDG资本、天创资本等国际顶级风投机构的投资。
					</p>
				</div>

        <!--index-container start-->
        <div class="index-container">
          <div class="am-g">
          	<#if artlist??>
          	<#list artlist as a>
            <div class="am-u-md-3">
              <div class="features_item">
								<img src="<#if (a.imgUrl != '' && a.imgUrl??)>${a.imgUrl}<#else>${base}/assets/images/news/b01.jpg</#if>" alt="">
								<h3 class="features_item--title">
								<a href="${base+category.htmlUrl}/${a.id}.html">
								<#if (a.title?length>14)>
								${a.title[0..14]}
								<#else>
								${a.title}
								</#if>
								</a>
								</h3>
								<p class="features_item--text">
									<#if (a.remark?length>28)>
									${a.remark[0..28]}
									<#else>
									${a.title}
									</#if>
								</p>
							</div>
            </div>
            </#list>
            </#if>
          </div>

          <div class="index-more">
            <button type="button" class="am-btn am-btn-secondary">MORE&nbsp;&nbsp;>></button>
          </div>
        </div>
        <!--index-container end-->
      </div>
    </div>

  </div>