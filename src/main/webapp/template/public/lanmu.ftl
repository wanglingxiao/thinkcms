<#import "/template/public/base.ftl" as my> 
<!--nav start-->
        <div class="nav-contain">
          <div class="nav-inner">
            <ul class="am-nav am-nav-pills am-nav-justify">
            	<@rootChannel loot=1;  lanmu>
            	<#if lanmu??>
            		<#list lanmu as c>
              			<li class=""><a href="<#if c.htmlUrl !='/'>${my.base+c.htmlUrl}/index.html<#else>${my.base}/index.html</#if>">${c.name}</a>
              			<#if c.childList??>
              				<ul class="sub-menu">
              					<#list c.childList as m>
			                  <li class="menu-item"><a href="${my.base+m.htmlUrl}/index.html">${m.name}</a></li>
			                  	</#list>
			                </ul>
              			</#if>
              			</li>
              		</#list>
              	</#if>
              	</@rootChannel>
            </ul>
          </div>
        </div>
        <!--nav end-->