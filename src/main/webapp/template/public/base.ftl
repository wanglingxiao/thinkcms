<#assign base = request.contextPath+"/html"/>
<#macro showtitle title len> 
    <#if (title?length>len)>
    	${title[0..len]}
    <#else>
    	${title}
    </#if>
  </#macro> 
  
<#macro mapCategory map>
<#if map['category']??>${map['category'].name}</#if>
</#macro>