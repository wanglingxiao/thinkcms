<div class="layout-container">
      <div class="index-page">
        <div data-am-widget="tabs" class="am-tabs am-tabs-default">
        <@slideImg limit=4; imglist>
          <div class="am-tabs-bd">
          	<#if imglist??>
          		<#assign x=0 />  
          		<#list imglist as c>
          		
            <div data-tab-panel-${x} class="am-tab-panel <#if x==0>am-active</#if>">
              <div class="index-banner" style="background:url(${c.imgUrl})">
                <div class="index-mask">
                  <div class="container">
                    <div class="am-g">
                      <div class="am-u-md-10 am-u-sm-centered">
                        <h1 class="slide_simple--title">${c.alt}</h1>
                        <p class="slide_simple--text am-text-left">
												  ${c.remark[0..15]?default("")}
											  </p>
                        <div class="slide_simple--buttons">
  												<button type="button" class="am-btn am-btn-danger">了解更多</button>
											  </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            	<#assign x=x+1 />
            	</#list>
            </#if>
            
          </div>

          <ul class="am-tabs-nav am-cf index-tab">
          	<#if imglist??>
          		<#assign y=0 />  
          		<#list imglist as c>
            <li <#if y==0>class="am-active"</#if>>
              <a href="[data-tab-panel-${y}] am-g">
                <div class="am-u-md-3 am-u-sm-3 am-padding-right-0">
                  <#if y==0>
                  <i class="am-icon-cog"></i>
                  <#elseif y==1>
                  <i class="am-icon-lightbulb-o"></i>
                  <#elseif y==2>
                  <i class="am-icon-line-chart"></i>
                  <#else>
                  <i class="am-icon-hourglass-end"></i>
                  </#if>
                  
                </div>
                <div class="school-item-right am-u-md-9 am-u-sm-9 am-text-left">
                  <strong class="promo_slider_nav--item_title">${c.alt}</strong>
                  <p class="promo_slider_nav--item_description">${c.remark[0..15]?default("")}</p>
                </div>
              </a>
            </li>
            <#assign y=y+1 />
            	</#list>
            </#if>
          </ul>
        </div>
		</@slideImg>
      </div>
    </div>