<#assign base = request.contextPath+"/html"/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${siteinfo.siteName}</title>
  <meta name="keywords" content="${siteinfo.keywords}" > 
  <meta  name="description" content="${siteinfo.description}">
  <link rel="bookmark"  type="image/x-icon"  href="${siteinfo.siteLogo}"/>
  <link rel="shortcut icon" href="${siteinfo.siteLogo}">
  <link rel="stylesheet" href="${base}/assets/css/amazeui.css" />
  <link rel="stylesheet" href="${base}/assets/css/common.min.css" />
  <link rel="stylesheet" href="${base}/assets/css/news.min.css" />

</head>
<body>
  <div class="layout">
    <!--===========layout-header================-->
    <div class="layout-header am-hide-sm-only">

      <div class="header-box" data-am-sticky>
        <#include "/template/public/head.ftl"/>


        <#include "/template/public/lanmu.ftl"/>
      </div>

    </div>
    <!--mobile header start-->
    <#include "/template/public/mobileHeader.ftl"/>
      <!--mobile header end-->
    </div>



    <!--===========layout-container================-->
    <div class="layout-container">
      <div class="page-header">
        <div class="am-container">
          <h1 class="page-header-title">${category.name}</h1>
        </div>
      </div>

      <div class="breadcrumb-box">
        <div class="am-container">
          <ol class="am-breadcrumb">
            <li><a href="${base}/index.html">首页</a></li>
            <#if parentCategory??>
            <#list parentCategory as c>
            <li><a href="${base+c.htmlUrl}/index.html">${c.name}</a></li>
            </#list>
            <#else>
            <li class="am-active">${category.name}</li>
            </#if>
          </ol>
        </div>
      </div>
    </div>

    <div class="section news-section">
      <div class="container">
        <!--news-section left start-->
        <div class="am-u-md-9">
          <div class="article">
            <header class="article--header">
              <h2 class="article--title"><a href="#" rel="">${art.title}</a></h2>
              <ul class="article--meta">
                <li class="article--meta_item"><i class="am-icon-calendar"></i>${art.createDate?string("yyyy-MM-dd")}</li>
                <li class="article--meta_item"><i class="am-icon-user"></i>by${art.author}</li>
              </ul>
            </header>
            <div class="article--content">
              ${art.content}
            </div>
          </div>

        </div>
        <!--news-section left end-->

        <!--news-section right start-->
        <div class="am-u-md-3">
          <div class="blog_sidebar">
            <div class="widget">
              <#if brotherCategory??>
			<h2 class="widget--title"><i class="am-icon-file-text-o"></i>${pCategory.name}</h2>
              				<ul>
              					<#list brotherCategory as b >
								<li><a href="${base+b.htmlUrl}/index.html">${b.name}</a></li>
								</#list>
							</ul>
			<#else>
			<h2 class="widget--title"><i class="am-icon-file-text-o"></i><a href="${base+category.htmlUrl}/index.html">${category.name}</a></h2>
			</#if>
            </div>
          </div>
        </div>
        <!--news-section right end-->
      </div>
    </div>
  </div>



  <!--===========layout-container================-->
  <#include "/template/public/foot.ftl"/>
  <script src="${base}/assets/js/jquery-2.1.0.js" charset="utf-8"></script>
  <script src="${base}/assets/js/amazeui.js" charset="utf-8"></script>
</body>

</html>
