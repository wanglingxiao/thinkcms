<#assign base = request.contextPath+"/html"/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${siteinfo.siteName}</title>
  <meta name="keywords" content="${siteinfo.keywords}" > 
  <meta  name="description" content="${siteinfo.description}">
  <link rel="bookmark"  type="image/x-icon"  href="${siteinfo.siteLogo}"/>
  <link rel="shortcut icon" href="${siteinfo.siteLogo}">  
  <link rel="stylesheet" href="${base}/assets/css/amazeui.css" />
  <link rel="stylesheet" href="${base}/assets/css/common.min.css" />
  <link rel="stylesheet" href="${base}/assets/css/news.min.css" />
</head>
<body>
  <div class="layout">
    <!--===========layout-header================-->
    <div class="layout-header am-hide-sm-only">

      <div class="header-box" data-am-sticky>
        <#include "/template/public/head.ftl"/>


        <#include "/template/public/lanmu.ftl"/>
      </div>

    </div>
    <!--mobile header start-->
    <#include "/template/public/mobileHeader.ftl"/>
      <!--mobile header end-->
    </div>




    <!--===========layout-container================-->
    <div class="layout-container">
      <div class="page-header">
        <div class="am-container">
          <h1 class="page-header-title">${category.name}</h1>
        </div>
      </div>

      <div class="breadcrumb-box">
        <div class="am-container">
          <ol class="am-breadcrumb">
            <li><a href="${base}/index.html">首页</a></li>
            <#if parentCategory??>
            <#list parentCategory as c>
            <li href="${base+c.htmlUrl}/index.html">${c.name}</li>
            </#list>
            <#else>
            <li class="am-active">${category.name}</li>
            </#if>
          </ol>
        </div>
      </div>
    </div>

    <div class="section news-section" >
      <div class="container">

		
        <div class="am-u-md-9">
		<style>
			.am-g ul li{
				list-style:none;
			}
			.am-g ul li span{
				float:right;
			}
		</style>
		
        <div class="news-contaier">
          <div class="blog">
            <div class="am-g">
			  <ul>
			  <#if artpage.items??>
            	<#list artpage.items as a>
            	<li><a href="${base+category.htmlUrl}/${a.id}.html">
            	<#if (a.title?length>40) >
                    ${a.title[0..40]}
                    <#else>
                    ${a.title}
                    </#if></a><span >${a.createDate?string('yyyy-MM-dd')}</span></li>
                 </#list>
              </#if>
			  </ul>
              
              
            </div>

            <!-- pagination-->
            <ul class="am-pagination">
              <#if (prePage == 1)>
              <li class="am-disabled"><a href="#">&laquo;</a></li>
              <#else>
               <li><a href="${base+category.htmlUrl}/index_${prePage}.html">&laquo;</a></li>
               </#if>
               <#list 1..totalPage as p>
              <li <#if (artpage.pageNo == p)>class="am-active"</#if>>
              <#if (p == 1)>
              <a href="${base+category.htmlUrl}/index.html">${p}</a></li>
              <#else>
              <a href="${base+category.htmlUrl}/index_${p}.html">${p}</a></li>
              </#if>
              </#list>
              <#if (nextPage == totalPage || nextPage == 1)>
              <li class="am-disabled"><a href="#">&raquo;</a></li>
              <#else>
              <li><a href="${base+category.htmlUrl}/index_${prePage}.html">&raquo;</a></li>
              </#if>
            </ul>
          </div>

        
		</div>
		</div>
		
		<div class="am-u-md-3">
          <div class="blog_sidebar">
            <div class="widget">
              <#if brotherCategory??>
			<h2 class="widget--title"><i class="am-icon-file-text-o"></i>${pCategory.name}</h2>
              				<ul>
              					<#list brotherCategory as b >
								<li><a href="${base+b.htmlUrl}/index.html">${b.name}</a></li>
								</#list>
							</ul>
			<#else>
			<h2 class="widget--title"><i class="am-icon-file-text-o"></i><a href="${base+category.htmlUrl}/index.html">${category.name}</a></h2>
			</#if>
            </div>
           
          </div>
        </div>


      </div>
    </div>



    <#include "/template/public/lxwm.ftl"/>


    <!--===========layout-footer================-->
    <#include "/template/public/foot.ftl"/>
  <script src="${base}/assets/js/jquery-2.1.0.js" charset="utf-8"></script>
  <script src="${base}/assets/js/amazeui.js" charset="utf-8"></script>
</body>

</html>
