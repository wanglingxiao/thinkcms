<#assign base = request.contextPath+"/html"/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${siteinfo.siteName}</title>
  <meta name="keywords" content="${siteinfo.keywords}" > 
  <meta  name="description" content="${siteinfo.description}">
  <link rel="bookmark"  type="image/x-icon"  href="${siteinfo.siteLogo}"/>
  <link rel="shortcut icon" href="${siteinfo.siteLogo}">
    
  <link rel="stylesheet" href="${base}/assets/css/amazeui.css" />
  <link rel="stylesheet" href="${base}/assets/css/common.min.css" />
  <link rel="stylesheet" href="${base}/assets/css/about.min.css" />
</head>
<body>
  <div class="layout">
    <!--===========layout-header================-->
    <div class="layout-header am-hide-sm-only">

      <div class="header-box" data-am-sticky>
        <#include "/template/public/head.ftl"/>


        <#include "/template/public/lanmu.ftl"/>
      </div>

    </div>
    <#include "/template/public/mobileHeader.ftl"/>
    </div>




    <!--===========layout-container================-->
    <div class="layout-container">
      <div class="page-header">
        <div class="am-container">
          <h1 class="page-header-title">${category.name}</h1>
        </div>
      </div>

      <div class="breadcrumb-box">
        <div class="am-container">
          <ol class="am-breadcrumb">
            <li><a href="${base}/index.html">首页</a></li>
            <#if parentCategory??>
            <#list parentCategory as c>
            <li href="${base+c.htmlUrl}/index.html">${c.name}</li>
            </#list>
            <#else>
            <li class="am-active">${category.name}</li>
            </#if>
          </ol>
        </div>
      </div>
    </div>


    <div class="section">
      <div class="container">

        <!--about-container start-->
        <div class="about-container">
          <div class="am-g">
          	<#if artpage??>
            <#if artpage.items??>
            	<#list artpage.items as a>
					${a.content}
					<#break>
				</#list>
			</#if>
           </#if> 
          </div>
        </div>
        <!--about-container end-->
      </div>
    </div>


    <hr class="section_divider -narrow">





  <#include "/template/public/lxwm.ftl"/>


  <!--===========layout-footer================-->
   <#include "/template/public/foot.ftl"/>
  <script src="${base}/assets/js/jquery-2.1.0.js" charset="utf-8"></script>
  <script src="${base}/assets/js/amazeui.js" charset="utf-8"></script>
</body>

</html>
