<#import "/template/public/base.ftl" as my> 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${siteinfo.siteName}</title>
  <meta name="keywords" content="${siteinfo.keywords}" > 
  <meta  name="description" content="${siteinfo.description}">
  <link rel="bookmark"  type="image/x-icon"  href="${siteinfo.siteLogo}"/>
  <link rel="shortcut icon" href="${siteinfo.siteLogo}">
  <link rel="stylesheet" href="${my.base}/assets/css/amazeui.css" />
  <link rel="stylesheet" href="${my.base}/assets/css/common.min.css" />
  <link rel="stylesheet" href="${my.base}/assets/css/index.min.css" />
</head>
<body>
  <div class="layout">
    <!--===========layout-header================-->
    <div class="layout-header am-hide-sm-only">


      <div class="header-box" data-am-sticky>
        <#include "/template/public/head.ftl"/>


        <#include "/template/public/lanmu.ftl"/>
      </div>
    </div>

    <!--mobile header start-->
    <#include "/template/public/mobileHeader.ftl"/>
    <!--mobile header end-->
    </div>




    <!--===========layout-container================-->
    <#include "/template/public/banner.ftl"/>


    <div class="section">
      <div class="container">
        <div class="section--header">
        			 <@myarticle cid=26 start=1 limit=4; map>
					<h2 class="section--title"><@my.mapCategory map=map /></h2>
					<p class="section--description">
					</p>
					
				</div>

        <!--index-container start-->
        <div class="index-container">
          <div class="am-g">
          
          
          	<#if map['articles']??>
          	<#list map['articles'] as a>
            <div class="am-u-md-3">
              <div class="features_item">
								<img style="width:262px;height:198px;" src="<#if (a.imgUrl != '' && a.imgUrl??)>${a.imgUrl}<#else>${my.base}/assets/images/news/b01.jpg</#if>" alt="">
								<h3 class="features_item--title">
								<a href="${my.base+map['category'].htmlUrl}/${a.id}.html">
								<@my.showtitle title="${a.title}" len=14 />
								</a>
								</h3>
								<p class="features_item--text">
									<@my.showtitle title="${a.remark}" len=28 />
								</p>
							</div>
            </div>
            </#list>
            </#if>
            </@myarticle>
          </div>

          <div class="index-more">
            <button type="button" class="am-btn am-btn-secondary">MORE&nbsp;&nbsp;>></button>
          </div>
        </div>
        <!--index-container end-->
      </div>
    </div>

  </div>


  <div class="section promo_banner-container">
    <!--index-container start-->
    <div class="promo_banner-box">
      <div class="container">
        <h2 class="promo_banner--title">期待您的合作</h2>
        <p class="promo_banner--text">
					上海英德仑斯科贸有限公司于二零零三年注册在杨浦科委的科技公司，主要代理国外的各种高新材料。
					<br> 于2015年在新加坡开办了海外公司。开办至今已经为各大船厂以及陆地项目提供了大量的配套产品，四技，施工服务，以及内装总包工程。
					<br> 在军船上也得到了应用，同时为东南亚的造船提供配套。在质量以及售后服务上，得到了客户的一致认可。连续几年被杨浦区评为A级企业。
        			<footer class="promo_banner--footer">
						<button type="button" class="am-btn am-btn-secondary" onclick="location.href='${base}/lxwm/index.html'">MORE>></button>
					</footer>
      </div>
    </div>
    <!--index-container end-->
  </div>





  <#include "/template/public/lxwm.ftl"/>




  <!--===========layout-footer================-->
  <#include "/template/public/foot.ftl"/>
  <script src="${my.base}/assets/js/jquery-2.1.0.js" charset="utf-8"></script>
  <script src="${my.base}/assets/js/amazeui.js" charset="utf-8"></script>
  <script src="${my.base}/assets/js/common.js" charset="utf-8"></script>
</body>

</html>
