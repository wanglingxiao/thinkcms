<#import "/template/public/base.ftl" as my>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${siteinfo.siteName}</title>
  <link rel="stylesheet" href="${my.base}/assets/css/amazeui.css" />
  <link rel="stylesheet" href="${my.base}/assets/css/common.min.css" />
  <link rel="stylesheet" href="${my.base}/assets/css/index.min.css" />
</head>
<body>
	<#if category.childList??>
		<#list category.childList as c>
			<script>
				window.location.href = '${c.htmlUrl}/index.html';
			</script>
			<#break>
		</#list>
	</#if>
</body>
</html>
