<#assign base = request.contextPath+"/html"/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${siteinfo.siteName}</title>
  <meta name="keywords" content="${siteinfo.keywords}" > 
  <meta  name="description" content="${siteinfo.description}">
  <link rel="bookmark"  type="image/x-icon"  href="${siteinfo.siteLogo}"/>
  <link rel="shortcut icon" href="${siteinfo.siteLogo}">
  <link rel="stylesheet" href="${base}/assets/css/amazeui.css" />
  <link rel="stylesheet" href="${base}/assets/css/common.min.css" />
  <link rel="stylesheet" href="${base}/assets/css/example.min.css" />
</head>
<body>
  <div class="layout">
    <!--===========layout-header================-->
    <div class="layout-header am-hide-sm-only">
      

      <div class="header-box" data-am-sticky>
        <#include "/template/public/head.ftl"/>


        <#include "/template/public/lanmu.ftl"/>
      </div>

    </div>
    <!--mobile header start-->
    <#include "/template/public/mobileHeader.ftl"/>
      <!--mobile header end-->
    </div>




    <!--===========layout-container================-->
    <div class="layout-container">
      <div class="page-header">
        <div class="am-container">
          <h1 class="page-header-title">${category.name}</h1>
        </div>
      </div>

      <div class="breadcrumb-box">
        <div class="am-container">
          <ol class="am-breadcrumb">
            <li><a href="${base}/index.html">首页</a></li>
            <#if parentCategory??>
            <#list parentCategory as c>
            <li><a href="${base+c.htmlUrl}/index.html">${c.name}</a></li>
            </#list>
            <#else>
            <li class="am-active">${category.name}</li>
            </#if>
          </ol>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">

        <div class="example-container">
          <div class="am-tabs" data-am-tabs>
            <ul class="am-tabs-nav am-nav am-nav-tabs am-g">
			  <#if brotherCategory??>
			  <#list brotherCategory as b>
              <li class="<#if (category.id == b.id)>am-active</#if> am-u-md-4"  style="float:left"><a href="#" onclick="javascript:window.location.href='${base+b.htmlUrl}/index.html'"><i class="am-icon-map-o"></i>${b.name}</a></li>
			  </#list>
			  </#if>
            </ul>
            <div class="am-tabs-bd am-tabs-bd-ofv">
              <div class="am-tab-panel am-active" >
                 <div class="am-g">

                   <#if artpage??>
            		<#if artpage.items??>
            		  <#list artpage.items as a>

                   <div class="am-u-lg-4 am-u-md-6 am-u-end">
                     <a href="${base+category.htmlUrl}/${a.id}.html" style="background-image: url('<#if (a.imgUrl != '' && a.imgUrl??)>${a.imgUrl}<#else>${base}/assets/images/news/b01.jpg</#if>');" class="example-item-bg"></a>
                    
					 <div class="article-header">
                    <h2><a href="${base+category.htmlUrl}/${a.id}.html" rel=""><@my.showtitle title="${a.title}" len=16 /></a></h2>
					</div>
                   </div>
                    </#list>
					</#if>
					</#if>

                   </div>
                 <!-- pagination-->
            <ul class="am-pagination" style="text-align: center;">
            <#if artpage??>
              <#if (prePage == 1)>
              <li class="am-disabled"><a href="#">&laquo;</a></li>
              <#else>
               <li><a href="${base+category.htmlUrl}/index_${prePage}.html">&laquo;</a></li>
               </#if>
               <#list 1..totalPage as p>
              <li <#if (artpage.pageNo == p)>class="am-active"</#if>>
              <#if (p == 1)>
              <a href="${base+category.htmlUrl}/index.html">${p}</a></li>
              <#else>
              <a href="${base+category.htmlUrl}/index_${p}.html">${p}</a></li>
              </#if>
              </#list>
              <#if (nextPage == totalPage || nextPage == 1)>
              <li class="am-disabled"><a href="#">&raquo;</a></li>
              <#else>
              <li><a href="${base+category.htmlUrl}/index_${prePage}.html">&raquo;</a></li>
              </#if>
             </#if>
            </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>





    <!--===========layout-footer================-->
    <#include "/template/public/foot.ftl"/>
  <script src="${base}/assets/js/jquery-2.1.0.js" charset="utf-8"></script>
  <script src="${base}/assets/js/amazeui.js" charset="utf-8"></script>
</body>

</html>
