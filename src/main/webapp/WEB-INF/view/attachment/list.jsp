<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page pageEncoding="UTF-8"%>
<div class="am-u-sm-12">
	<form class="am-form">
		<table class="am-table am-table-striped am-table-hover table-main">
			<thead>
				<tr>
					<th class="table-check"><input type="checkbox" id="checkAll" onclick="selectAll();" /></th>
					<th class="table-id">ID</th>
					<th class="table-title">文件名</th>
					<th class="table-title">文件类型</th>
					<th class="table-title am-hide-sm-only">文件URL</th>
					<th class="table-title am-hide-sm-only">文件大小</th>
					<th class="table-date am-hide-sm-only">创建日期</th>
					<th class="table-set">操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${list.items }" var="item">
					<tr>
						<td><input type="checkbox" name="checkitem" onclick="selectItem(this);" id="check${item.id }" value="${item.id }"/></td>
						<td>${item.id }</td>
						<td>${item.fileName }</td>
						<td>${item.fileType }</td>
						<c:choose>
							<c:when test="${fn:contains(item.fileType, 'image')}">
								<td class="am-hide-sm-only"><img alt="" src="${item.url }" width="200px"></td>
							</c:when>
							<c:otherwise>
								<td class="am-hide-sm-only">${item.url }</td>
							</c:otherwise>
						</c:choose>
						
						<td class="am-hide-sm-only">${item.fileSize }</td>
						<td class="am-hide-sm-only">${item.createDate }</td>
						<td>
							<div class="am-btn-toolbar">
								<div class="am-btn-group am-btn-group-xs">
									<button type="button" class="am-btn am-btn-default am-text-danger"
									    onclick="del('${item.id}')">
										<span class="am-icon-trash-o"></span> 删除
									</button>
								</div>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		${list.pageStr }
		<hr />
		<p>注：.....</p>
	</form>
</div>
