<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->
<body>
	<!-- content start -->
	<div class="admin-content">
		<div class="admin-content-body">
			<div class="am-cf am-padding am-padding-bottom-0">
				<div class="am-fl am-cf">
					<strong class="am-text-primary am-text-lg">栏目管理</strong> / <small>Category</small>
				</div>
			</div>

			<hr>

			<div class="am-g">
				<form id="form">
					<div class="am-u-sm-12 am-u-md-6">
						<div class="am-btn-toolbar">
							<div class="am-u-sm-8 ">
								<button type="button" class="am-btn am-btn-secondary"
									onclick="openWindow('category/add','栏目新增');">
									<span class="am-icon-plus"></span> 新增
								</button>
								<button type="button" class="am-btn am-btn-warning"
									onClick="del();">
									<span class="am-icon-trash-o"></span> 删除
								</button>
								<button class="am-btn am-btn-primary" type="reset"
									onClick="allHtml();">
									<span class="am-icon-refresh"></span> 生成全部静态页面
								</button>
							</div>
						</div>
					</div>
					<div class="am-u-sm-12 am-u-md-3">
						<div class="am-input-group am-input-group-sm">
							<input type="text" class="am-form-field" id="description" name="description" placeholder="栏目名称模糊查询">
							<span class="am-input-group-btn">
								<button class="am-btn am-btn-default" type="button" onclick="query()">搜索</button>
							</span>
						</div>
					</div>
				</form>
			</div>
			<div id="list" class="am-g"></div>
		</div>
		<%@ include file="/WEB-INF/view/common/footer.jsp"%>
	</div>
	<!-- content end -->
	<script type="text/javascript">
		var oTimer = null;
		var tempstr;
		$(function() {
			query();
		});
		function query(pageNo,pageSize) {
			$.ajax({
						url : 'category/list',
						type : 'post',
						data : {
							pageNo:pageNo,
							pageSize:pageSize
						},
						cache : false,
						contentType : "application/x-www-form-urlencoded; charset=utf-8",
						success : function(html) {
							$('#list').html(html);
						},
						error : function() {
							layer.msg('系统异常');
						}
					});
		}
		
		function allHtml(){
			layer.load(0, {shade: false});
			oTimer = setInterval("getProgress()", 100);
			$.ajax({
				url    : 'category/allHtml',
				type : 'post',
				success:function(data){
					layer.closeAll('loading');
					window.clearInterval(oTimer);
					if (data.success) {
						layer.msg("生成成功");
					} else {
						layer.msg('生成失败');
					}
				}
			});
		}
		
		function getProgress() {
			var now = new Date();
		    $.ajax({
		        type: "post",
		        dataType: "json",
		        url: "category/progress",
		        data: now.getTime(),
		        success: function(data) {
		        	if(data != tempstr){
		        		var count = $("#message").find('p').size();
		        		$("#message").prepend(" <p>"+data+"</p>");	
		        		if(count>4){
		        			$("#message p:last").remove();
		        		}
		        		
		        	}
		        	tempstr = data;
		        },
		        error: function(err) {
		        }
		    });
		}
		
		function createhtml(id){
			layer.msg('你确定要生成静态页面么？', {
				  time: 0 //不自动关闭
				  ,btn: ['确定', '取消']
				  ,yes: function(index){
					  layer.closeAll();
					  $("#message").html("");
					  layer.load(0, {shade: false});
					  oTimer = setInterval("getProgress()", 100);
					  $.ajax({
							url : 'category/crateHTML',
							type : 'post',
							data : {
								id : id
							},
							cache : false,
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(html) {
								layer.closeAll('loading');
								window.clearInterval(oTimer);
								if (html.success) {
									layer.msg("生成成功");
								} else {
									layer.msg('生成失败');
								}
							},
							error : function() {
								layer.msg('系统异常');
							}
						});//end ajax
					  
				  }//end yes function
				});//end layer.msg
		}
		
		function del(id) {
			if (!id) {
				id = "";
				$("[name='checkitem'][checked]").each(function(index) {
					id += $(this).val();
					if (index < $("[name='checkitem'][checked]").length - 1) {
						id += ",";
					}
				});
				if(id.length<1){
					layer.msg("请选择要删除的选项");
					return;
				}
			}
			layer.msg('你确定要删除么？', {
				  time: 0 //不自动关闭
				  ,btn: ['确定', '取消']
				  ,yes: function(index){
					 
					  $.ajax({
							url : 'category/del',
							type : 'post',
							data : {
								ids : id
							},
							cache : false,
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(html) {
								if (html.success) {
									layer.msg("删除成功");
									query();
								} else {
									layer.msg('删除失败');
								}
							},
							error : function() {
								layer.msg('系统异常');
							}
						});//end ajax
					  
				  }//end yes function
				});//end layer.msg
			
		}
		function selectAll() {
			$('input:checkbox').prop("checked",
					document.getElementById("checkAll").checked);
			$('input:checkbox').attr("checked",
					document.getElementById("checkAll").checked);
		}
		function selectItem(obj) {
			$(obj).prop("checked",
					document.getElementById($(obj).attr("id")).checked);
			$(obj).attr("checked",
					document.getElementById($(obj).attr("id")).checked);
		}
	</script>
</body>
</html>
