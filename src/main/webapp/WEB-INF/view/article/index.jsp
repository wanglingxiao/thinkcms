<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->
<link rel="stylesheet" type="text/css"	href='<c:url value="/static/css/zTreeStyle.css"/>'>
<link rel="stylesheet" type="text/css"	href='<c:url value="/static/css/amazeui.datatables.min.css"/>'>
<script type="text/javascript"  src="<c:url value="/static/js/amazeui.datatables.min.js"/>"></script>	
<script type="text/javascript"  src="<c:url value="/static/js/dataTables.responsive.min.js"/>"></script>	
<script type="text/javascript"  src="<c:url value="/static/js/jquery.ztree.core.js"/>"></script>	
<style type="text/css">  
    .ztree li span.button.ico_docu {  
        background-position: -110px 0;  
        margin-right: 2px;  
        vertical-align: top;  
}  
</style>
<body>
	<!-- content start -->
	<div class="admin-content">
		<div class="admin-content-body">
			<div class="am-cf am-padding am-padding-bottom-0">
				<div class="am-fl am-cf">
					<strong class="am-text-primary am-text-lg">文章管理</strong> / <small>Article</small>
				</div>
			</div>

			<hr>

			<div class="am-g">
				<form id="form">
					<div class="am-u-sm-12 am-u-md-6">
						<div class="am-btn-toolbar">
							<div class="am-u-sm-8 ">
								<button type="button" class="am-btn am-btn-secondary"
									onclick="openAddWindow('article/add','文章新增');">
									<span class="am-icon-plus"></span> 新增
								</button>
								<button type="button" class="am-btn am-btn-warning"
									onClick="del();">
									<span class="am-icon-trash-o"></span> 删除
								</button>
							</div>
						</div>
					</div>
					<div class="am-u-sm-12 am-u-md-3">
					</div>
				</form>
			</div>
			<ul id="treeDemo" class="ztree am-u-sm-2"  style="margin-top:20px"></ul>
			<div id="list" class="am-u-sm-10 am-g">
				<div class="am-u-sm-12">
					<table width="100%" class="am-table am-table-striped am-table-bordered am-table-compact am-text-nowrap" id="example">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" id="checkAll" onclick="selectAll();" /></th>
								<th class="table-id">ID</th>
								<th class="table-title">文章标题</th>
								<th class="table-title">所属栏目</th>
								<th class="table-title am-hide-sm-only">文章标签</th>
								<th class="table-date am-hide-sm-only">是否可用</th>
								<th class="table-set">操作</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<hr />
					<p>注：.....</p>
			</div>
			
			</div>
		</div>
		<%@ include file="/WEB-INF/view/common/footer.jsp"%>
	</div>
	<!-- content end -->
	<script type="text/javascript">
		var zTreeObj;
		$(function() {
			query();
			setZTree();
		});
		
		function query(categoryId){
			var url = "article/list";
			$("#example").dataTable({
				"bPaginate" : true,		//开启分页
				"bLengthChange" : 10,	// 每行显示记录数  
                "iDisplayLength" : 6,		// 每页显示行数
				"bLengthChange":true,//是否显示一个每页长度的选择条
				"aLengthMenu":[[1, 3, 6, -1], [1, 3, 6, "All"]],	//第一个为显示每页条目数的选项，第二个是关于这些选项的解释
				"bAutoWidth": true, 		//是否自适应宽度
				"bProcessing":true,		//是否显示“正在处理”这个提示信息
				"bSort":false,				//是否让各列具有按列排序功能
				"bStateSave":true,			//是否打开客户端状态记录功能
				"bDestroy":true,			//dataTable绑定时，将之前的那个数据对象清除掉，换以新的对象设置
				"serverSide":true,			//开启服务器处理模式
				"sAjaxSource":url,			//请求资源路径
				/*
				 使用ajax，在服务端处理数据
				 sSource:即是"sAjaxSource"
				 aoData:要传递到服务端的参数
				 fnCallback:处理返回数据的回调函数
				 */
				 "fnServerData": function(sSource, aoData, fnCallback){
					 var data = {"aodata" : JSON.stringify(aoData)};
					 if(categoryId != null && categoryId != ""){
						 data["categoryId"] = categoryId;
						 //data["search_name"] = '测试名称';
						 //data["search_age"] = 28;
						 //data["search_score"] = 30;
					 }
					  $.ajax( { 
						  'type' : 'POST', 
						  "url": sSource, 
						  "dataType": "json", 
						  "data": data,
						  "success": function(resp) { 
						   		fnCallback(resp);
						   } 
					  }); 
				 }, 
				 "columns": [
						        { "data": "id" },
						        { "data": "id" },
						        { "data": "title" },
						        { "data": "categoryName" },
						        { "data": "tag" },
						        { "data": "available" },
						        { "data": "id" }
						    ],
						    "columnDefs": [{
						        targets: 0,
						        render: function(data, type, row, meta) {
						            return '<input type="checkbox" name="checkitem" onclick="selectItem(this);" id="check'+data+'" value="'+data+'"/>';
						        }
						    },{
						    	targets: 6,
						        render: function(data, type, row, meta) {
						            return '<div class="am-btn-toolbar">'+
									'<div class="am-btn-group am-btn-group-xs">'+
									'<button type="button" class="am-btn am-btn-default am-text-secondary"'+
										'onclick=openAddWindow("article/edit?id='+data+'","文章编辑");>'+
										'<span class="am-icon-pencil-square-o"></span> 编辑'+
									'</button>'+
									'<button type="button" class="am-btn am-btn-default am-text-danger"'+
									    'onclick=del("'+data+'")>'+
										'<span class="am-icon-trash-o"></span> 删除'+
									'</button>'+
								'</div>'+
							'</div>';
						        }
						    }]
			});
		}
		
		
		function setTree() {
			$('#tree').treeview({
				data : '${treeData}'
			});
			$('#tree').on(
					'nodeSelected',
					function(event, data) {
						var categoryId = $('#tree').treeview('getSelected')[0].tags;
						layer.msg("您点击的栏目ID为:"+categoryId);
					});
		}
		
		function setZTree(){
			 var setting = {
					 view: {
						fontCss : {"font-family": "微软雅黑", "font-size": "16px"}
					 },
					callback: {
						onClick: function(event, treeId, treeNode){
							query(treeNode.categoryId);
						}
					}
				};
			 var zNodes = JSON.parse('${treeData}');
			 zTreeObj = $.fn.zTree.init($("#treeDemo"), setting,  zNodes);
		}
		
		function del(id) {
			if (!id) {
				id = "";
				$("[name='checkitem'][checked]").each(function(index) {
					id += $(this).val();
					if (index < $("[name='checkitem'][checked]").length - 1) {
						id += ",";
					}
				});
				if(id.length<1){
					layer.msg("请选择要删除的选项");
					return;
				}
			}
			layer.msg('你确定要删除么？', {
				  time: 0 //不自动关闭
				  ,btn: ['确定', '取消']
				  ,yes: function(index){
					 
					  $.ajax({
							url : 'article/del',
							type : 'post',
							data : {
								ids : id
							},
							cache : false,
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(html) {
								if (html.success) {
									layer.msg("删除成功");
									var checkedNodes = zTreeObj.getSelectedNodes();
									if(checkedNodes.length>0){
										var categoryId = checkedNodes[0].categoryId;
										if(categoryId != null && categoryId != 0 && categoryId != ''){
											query(categoryId);
										}
									}else{
										query();
									}
								} else {
									layer.msg('删除失败');
								}
							},
							error : function() {
								layer.msg('系统异常');
							}
						});//end ajax
					  
				  }//end yes function
				});//end layer.msg
			
		}
		function selectAll() {
			$('input:checkbox').prop("checked",
					document.getElementById("checkAll").checked);
			$('input:checkbox').attr("checked",
					document.getElementById("checkAll").checked);
		}
		function selectItem(obj) {
			$(obj).prop("checked",
					document.getElementById($(obj).attr("id")).checked);
			$(obj).attr("checked",
					document.getElementById($(obj).attr("id")).checked);
		}
		function openAddWindow(url, title) {
			var checkedNodes = zTreeObj.getSelectedNodes();
			if(checkedNodes.length>0){
				var categoryId = checkedNodes[0].categoryId;
				if(categoryId != null && categoryId != 0 && categoryId != ''){
					if(url.indexOf("?") != -1){
						url = url + "&categoryId="+categoryId;
					}else{
						url = url + "?categoryId="+categoryId;
					}
					
				}
			}
			var index = layer.open({
				type : 2,
				title : title,
				area : [ '80%', '80%' ],
				fix : false, //不固定
				maxmin : true,
				content : url
			});
		}
	</script>
</body>
</html>
