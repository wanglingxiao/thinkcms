<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<link rel="stylesheet" type="text/css"	href='<c:url value="/static/css/amazeui.datatables.min.css"/>'>
<script type="text/javascript"  src="<c:url value="/static/js/amazeui.datatables.min.js"/>"></script>	
<script type="text/javascript"  src="<c:url value="/static/js/dataTables.responsive.min.js"/>"></script>	
<!-- content start -->
	<div class="admin-content-body">
		<div class="am-cf am-padding">
			<div class="am-fl am-cf">
				<strong class="am-text-primary am-text-lg">首页</strong> / <small>网站后台~</small>
			</div>
		</div>
		<ul
			class="am-avg-sm-1 am-avg-md-4 am-margin am-padding am-text-center admin-content-list ">
			<li>
			<a href="#" class="am-text-success"><span
					class="am-icon-btn am-icon-file-text"></span><br />栏目<br />${categoryCount }</a></li>
			<li><a href="#" class="am-text-warning"><span
					class="am-icon-btn am-icon-briefcase"></span><br />文章<br />${articleCount }</a></li>
			<li><a href="#" class="am-text-danger"><span
					class="am-icon-btn am-icon-recycle"></span><br />幻灯图片<br />${slideImgCount }</a></li>
			<li><a href="#" class="am-text-secondary"><span
					class="am-icon-btn am-icon-user-md"></span><br />附件<br />${attachmentCount }</a></li>
		</ul>
		<shiro:hasRole name="admin">
		<div class="am-g">
			<div class="am-u-sm-12">
				<table width="100%" class="am-table am-table-striped am-table-bordered am-table-compact am-text-nowrap" id="example">
						<thead>
							<tr>
								<th class="table-id">会话ID</th>
								<th class="table-title">用户名</th>
								<th class="table-title">主机地址</th>
								<th class="table-title">开始访问时间</th>
								<th class="table-title">最后访问时间</th>
								<th class="table-set">操作</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
			</div>
		</div>
		</shiro:hasRole>

		</div>
	</div>

	<footer class="admin-content-footer">
		<hr>
		<p class="am-padding-left">© 2014 AllMobilize, Inc. Licensed under
			MIT license.</p>
	</footer>
<!-- content end -->
<script>
$(function() {
	online();
});
function online(){
	var url = "admin/sessions";
	$.ajax({
	    "url":url,
	    "type":"GET",
	    "success":function(result){
	    	if(result.success){
	    		var data = result.obj;
	    		$("#example").dataTable({
	    			"data":data,
	    			"columns": [
	    				        { "data": "id" },
	    				        { "data": "username" },
	    				        { "data": "host" },
	    				        { "data": "startTimestamp" },
	    				        { "data": "lastAccessTime" },
	    				        { "data": "id" }
	    				    ],
	    		    "columnDefs": [{
	    		    	targets: 5,
	    		        render: function(data) {
	    		            return '<div class="am-btn-toolbar">'+
	    					'<div class="am-btn-group am-btn-group-xs">'+
	    					'<button type="button" class="am-btn am-btn-default am-text-danger"'+
	    						'onclick=test("'+data+'");>'+
	    						'<span class="am-icon-pencil-square-o"></span> 强制退出'+
	    					'</button>'+
	    					'</div>'+
	    					'</div>';
	    		        }
	    		    }]
	    		});
	    	}
	    }
	});
}

function test(id){
	layer.confirm('你确定要强制退出该用户么？', {
		  time: 0 //不自动关闭
		  ,btn: ['确定', '取消']
		  ,yes: function(index){
				var url = "admin/forceLogout"
				$.ajax({
				    "url":url,
				    "data":{"sessionId":id},
				    "type":"POST",
				    "success":function(result){
				    	if(result.success){
				    		layer.msg("强制退出成功");
				    	}else{
				    		layer.msg("强制退出失败");
				    	}
				    	window.location.href="admin"
				    }
				});//end ajax
		  }//end yes function
	});//end layer.msg
}
</script>

