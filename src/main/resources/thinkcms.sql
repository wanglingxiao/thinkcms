/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : thinkcms

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2017-04-19 10:04:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_article
-- ----------------------------
DROP TABLE IF EXISTS `sys_article`;
CREATE TABLE `sys_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) DEFAULT NULL,
  `available` bit(1) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `content` longtext,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `show_order` int(11) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `attr` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_article
-- ----------------------------
INSERT INTO `sys_article` VALUES ('1', '', '', '26', '产品中心', '<p>文章内容</p>', '2017-03-23 18:06:59', '2017-03-24 00:00:00', '', null, '', '产品内容', '4', 'http://om6onp8sp.bkt.clouddn.com/c43c41f1-98f9-425e-9882-d863eebb0415.gif');
INSERT INTO `sys_article` VALUES ('5', '', '', '29', '新闻中心', '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1.6rem; color: rgb(150, 150, 150); font-family: &#39;Segoe UI&#39;, &#39;Lucida Grande&#39;, Helvetica, Arial, &#39;Microsoft YaHei&#39;, FreeSans, Arimo, &#39;Droid Sans&#39;, &#39;wenquanyi micro hei&#39;, &#39;Hiragino Sans GB&#39;, &#39;Hiragino Sans GB W3&#39;, FontAwesome, sans-serif; line-height: 23px; white-space: normal; text-align: center; background-color: rgb(255, 255, 255);\"><span style=\"font-size: 36px;\"><strong>2333</strong></span></p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1.6rem; color: rgb(150, 150, 150); font-family: &#39;Segoe UI&#39;, &#39;Lucida Grande&#39;, Helvetica, Arial, &#39;Microsoft YaHei&#39;, FreeSans, Arimo, &#39;Droid Sans&#39;, &#39;wenquanyi micro hei&#39;, &#39;Hiragino Sans GB&#39;, &#39;Hiragino Sans GB W3&#39;, FontAwesome, sans-serif; line-height: 23px; white-space: normal; background-color: rgb(255, 255, 255);\">7月19日，云适配（美通云动（北京）科技有限公司）今日宣布，由中国通信标准化协会管理，工业和信息化部业务指导下的数据中心联盟与移动智能终端技术创新与产业联盟发起的移动信息化可信选型认证（第一批）评选中，云适配两大产品Enterplorer企业浏览器、Enterplorer Studio开发工具，通过了移动企业应用平台的认证，成为“第一批”获得移动信息化可信选型认证的两大产品。该结果已于今天上午在北京召开“2016移动智能终端峰会新闻发布会暨移动信息化可信选型认证结果发布会”上进行了通报。</p><p style=\"text-align: center;\"><br/><img src=\"http://om6onp8sp.bkt.clouddn.com/d130b3d8-0d8f-45be-a04b-0ab1df9b1f9d.jpg\" alt=\"d130b3d8-0d8f-45be-a04b-0ab1df9b1f9d.jpg\"/></p><p style=\"box-sizing: border-box; margin-top: 1.6rem; margin-bottom: 1.6rem; color: rgb(150, 150, 150); font-family: &#39;Segoe UI&#39;, &#39;Lucida Grande&#39;, Helvetica, Arial, &#39;Microsoft YaHei&#39;, FreeSans, Arimo, &#39;Droid Sans&#39;, &#39;wenquanyi micro hei&#39;, &#39;Hiragino Sans GB&#39;, &#39;Hiragino Sans GB W3&#39;, FontAwesome, sans-serif; line-height: 23px; white-space: normal; background-color: rgb(255, 255, 255);\">据了解，移动信息化可信选型认证是由工业和信息化部指导，数据中心联盟组织，中国信息通信研究院测试评估面向移动办公、移动应用、移动开发等领域新发起的一个评估认证，是目前国内唯一针对移动信息化选型可信性的权威认证体系，为用户选择可信赖的移动信息化解决方案提供了很好的依据。它是继数据中心联盟推出可信云认证之后，在移动办公、移动信息化领域开辟的又一个权威认证。</p><p style=\"text-align: center;\"><img src=\"http://om6onp8sp.bkt.clouddn.com/9224c99f-beed-4061-98dd-0c7b0c0f35eb.jpg\" alt=\"9224c99f-beed-4061-98dd-0c7b0c0f35eb.jpg\"/></p><p style=\"box-sizing: border-box; margin-top: 1.6rem; margin-bottom: 1.6rem; color: rgb(150, 150, 150); font-family: &#39;Segoe UI&#39;, &#39;Lucida Grande&#39;, Helvetica, Arial, &#39;Microsoft YaHei&#39;, FreeSans, Arimo, &#39;Droid Sans&#39;, &#39;wenquanyi micro hei&#39;, &#39;Hiragino Sans GB&#39;, &#39;Hiragino Sans GB W3&#39;, FontAwesome, sans-serif; line-height: 23px; white-space: normal; background-color: rgb(255, 255, 255);\">据了解，云适配企业级浏览器Enterplorer，融合了HTML5标准以及双渲染引擎技术，在“NO APP、NO API”的前提下，摒弃了企业在移动化过程中开发多个原生APP的传统方式，帮助企业在一周之内开始移动办公模式。</p><p style=\"box-sizing: border-box; margin-top: 1.6rem; margin-bottom: 1.6rem; color: rgb(150, 150, 150); font-family: &#39;Segoe UI&#39;, &#39;Lucida Grande&#39;, Helvetica, Arial, &#39;Microsoft YaHei&#39;, FreeSans, Arimo, &#39;Droid Sans&#39;, &#39;wenquanyi micro hei&#39;, &#39;Hiragino Sans GB&#39;, &#39;Hiragino Sans GB W3&#39;, FontAwesome, sans-serif; line-height: 23px; white-space: normal; background-color: rgb(255, 255, 255);\">Enterplorer Studio是一套面向企业级移动信息化建设的开发平台。集聚开发、测试、打包、发布于一体的移动化开发综合平台。用户无需更改任何原有系统后台流程逻辑，无需为移动应用单独开发后台，实现对原有业务系统的零依赖。开发者无需经过复杂的编程，开发平台集成丰富的组件库，通过组件拖拽方式即可帮助企业将复杂的业务系统高效快速的适配到移动终端，极大减小了企业办公系统移动化的难度，加快企业办公移动化的脚步。</p><p style=\"box-sizing: border-box; margin-top: 1.6rem; margin-bottom: 1.6rem; color: rgb(150, 150, 150); font-family: &#39;Segoe UI&#39;, &#39;Lucida Grande&#39;, Helvetica, Arial, &#39;Microsoft YaHei&#39;, FreeSans, Arimo, &#39;Droid Sans&#39;, &#39;wenquanyi micro hei&#39;, &#39;Hiragino Sans GB&#39;, &#39;Hiragino Sans GB W3&#39;, FontAwesome, sans-serif; line-height: 23px; white-space: normal; background-color: rgb(255, 255, 255);\">云适配创始人兼CEO陈本峰表示：“移动化已经成为企业发展的标配，随着移动技术的深入发展，国家和企业对移动化服务的安全和可靠性的重视程度在不断加深，这也是移动信息化可信选型认证出台的最关键的原因。云适配凭借全球领先的移动化技术和专业的安全能力与经验，将HTML5技术最高效的应用在了企业移动化进程中。未来，云适配将继续发挥在浏览器和HTML5技术方面的领先优势，为用户提供安全的、高效的企业移动化服务。”</p><p><br/></p>', '2017-03-23 17:08:52', '2017-03-23 17:08:28', '', null, '', '测试巨量内容个文章', null, 'http://om6onp8sp.bkt.clouddn.com/fc8b07df-2fa3-4e15-9f0d-ffd6fa8ca2e3.jpg');
INSERT INTO `sys_article` VALUES ('6', '', '', '29', '新闻中心', '<p>1111</p>', '2017-03-22 12:12:23', '2017-03-23 17:17:18', '', null, '', '111', null, 'http://om6onp8sp.bkt.clouddn.com/c43c41f1-98f9-425e-9882-d863eebb0415.gif');
INSERT INTO `sys_article` VALUES ('7', '', '', '29', '新闻中心', '<p><br/></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><span style=\"color:#ff0000\">本文来源于NGACN，作者：喃兑啊内；原文地址：<a href=\"http://bbs.ngacn.cc/read.php?tid=11238287\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">【点我查看】</a>转载请注明出处！</span></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　上次的神器篇好像没什么人看啊，嗨呀，这是最气的。这次是虚空碎片篇!</p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　更多7.2信息，可以看这里<a href=\"http://bbs.ngacn.cc/thread.php?fid=-16130685\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\"><span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(51, 153, 102);\"><strong style=\"list-style: none; padding: 0px; margin: 0px;\">麦田里的坑</strong></span></a></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　<strong style=\"list-style: none; padding: 0px; margin: 0px;\">相关帖子——</strong></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　<a href=\"http://wow.178.com/201702/280707484263.html\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\"><span style=\"text-decoration:underline;\"><span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(0, 0, 255);\">7.2PTR Sentinax 歼星舰歼灭战事件内容和视频，可换取制造业橙装所需物品虚空碎片</span></span></a></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　<a href=\"http://wow.178.com/201703/283029230805.html\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\"><span style=\"text-decoration:underline;\"><span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(0, 0, 255);\">[本是同根生，相肝何太急]虚空碎片所需的数量大增!已全面成为最重要的新货币，赌橙需5000碎片一次了</span></span></a></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; text-align: center; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><img src=\"http://img4.178.com/wow/201703/284154447192/o_284155048855.jpg\" alt=\"\"/></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　虚空碎片经过PTR几次补丁已经完全变样了，变得和6.0德拉诺版本的“<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(0, 0, 255);\"><a href=\"http://wow.178.com/201411/210880119454.html\" class=\"kw_lnk\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">埃匹希斯水晶</a></span>”非常像了。然后又染上了D3的味道，变得没有上限，可以大量刷取，又因为新的赌博机制。和D3的“<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(255, 0, 0);\">血岩碎片</span>”也是非常的像。</p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(0, 0, 255);\"><a href=\"http://wow.178.com/201411/210880119454.html\" class=\"kw_lnk\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">埃匹希斯水晶</a></span>可以购买怨毒装备，但是装等很低，用物品可以升级。然并卵，在初期有用，中后期根本没人用怨毒装备。<a href=\"http://wow.178.com/201411/210880119454.html\" class=\"kw_lnk\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">埃匹希斯水晶</a>还可以换坐骑和其他一些东西，在游戏初期因为<a href=\"http://wow.178.com/list/211058428152.html\" class=\"kw_lnk\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">橙戒</a>任务等，获取方式缺乏，非常的缺，但是在游戏后期根本用不光!!!</p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; text-align: center; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><img src=\"http://img1.178.com/wow/201703/284154447192/o_284155048858.png\" alt=\"\"/><img src=\"http://img4.178.com/wow/201703/284154447192/o_284155048867.jpg\" alt=\"\"/></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(255, 0, 0);\">血岩碎片</span>，D3中的货币，也可以赌博换取对应位置的装备。出的装备非常随机，有灰色，蓝色，黄色甚至暗金套装。搏一搏，单车变摩托。刷大米小米各种都会出，有了就会赌博用掉，所以在游戏的前中后期都会有用处。</p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; text-align: center; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\"><img src=\"http://img3.178.com/wow/201703/284154447192/o_284155048875.jpg\" alt=\"\"/><img src=\"http://img4.178.com/wow/201703/284154447192/o_284155048882.jpg\" alt=\"\"/></p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　而<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(51, 153, 102);\">虚空碎片</span>，最开始是作为前夕的突袭事件任务掉落的货币物品，可以购买一些玩具之类的。在7.2全面整合了“<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(0, 0, 255);\"><a href=\"http://wow.178.com/201411/210880119454.html\" class=\"kw_lnk\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">埃匹希斯水晶</a></span>”和“<span style=\"list-style: none; padding: 0px; margin: 0px; color: rgb(255, 0, 0);\">血岩碎片</span>”的效果。</p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">　　可以购买各种各样的物品，可以购买对应位置的装备，装备可以搏一搏出核心橙!让虚空碎片，不会像<a href=\"http://wow.178.com/201411/210880119454.html\" class=\"kw_lnk\" style=\"list-style: none; padding: 0px; margin: 0px; text-decoration: none;\">埃匹希斯水晶</a>一样在后期无用，也不会像血岩碎片一样只是服务于装备系统。</p><p style=\"margin-top: 0px; margin-bottom: 1em; padding: 0px; vertical-align: baseline; font-family: SimSun; font-size: 14px; list-style: none; line-height: 1.8em; word-break: break-word; color: rgb(51, 51, 51); white-space: normal; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">折叠代码：</p><p><br/></p>', '2017-03-22 23:15:55', '2017-04-18 09:21:39', '文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要文章摘要', null, '', '222', null, 'http://om6onp8sp.bkt.clouddn.com/fc8b07df-2fa3-4e15-9f0d-ffd6fa8ca2e3.jpg');
INSERT INTO `sys_article` VALUES ('8', '', '', '29', '新闻中心', '<h1 style=\"margin: 30px 0px 5px; padding: 0px; border: 0px; outline: 0px; font-weight: normal; color: rgb(243, 230, 208); font-size: 24px; font-family: Arial, &#39;Microsoft Yahei&#39;, Helvetica, sans-serif; white-space: normal; background-color: rgb(18, 17, 15);\">暗黑3补丁2.5.0美服率先上线 版本更新改动一览</h1><p>2017-03-22 09:34:32 作者： 夜叶	来源:&nbsp;<a target=\"_blank\" class=\"epgY\" href=\"http://d.163.com/\" style=\"outline: none; text-decoration: none; color: rgb(185, 157, 110); padding: 0px 5px;\">凯恩之角</a>&nbsp;&nbsp;</p><p><span class=\"title\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px;\">分享到<span class=\"ep-poplist-arr\" style=\"margin: 0px; padding: 0px; border-width: 5px 5px 0px; border-style: solid dashed dashed; border-color: rgb(136, 136, 136) transparent transparent; outline: 0px; position: absolute; z-index: 10; font-size: 0px; height: 0px; width: 0px; line-height: 0; transition: all 0.25s ease 0s; top: 3px; right: 5px;\"></span></span><br/></p><p><a href=\"http://i.play.163.com/\" target=\"_blank\" class=\"info-app-push\" style=\"outline: none; text-decoration: none; color: rgb(186, 38, 54); padding: 0px 5px 0px 35px; position: relative;\">爱玩App订阅</a></p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"暗黑3补丁2.5.0美服率先上线 版本更新改动一览\" src=\"http://img3.cache.netease.com/game/2017/3/22/2017032209320701b21.jpg\" width=\"700\"/><br/></p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">3月22日凌晨美服已上线全新的2.5.0补丁，在这个补丁中改动/加入了四大功能，分别是装备库、锻造材料仓库、悬赏奖励章节移除以及太古装备。按照惯例，国服将在本周四（3月23日）的维护后上线该补丁，如果你想了解该补丁的详细情况，欢迎进入我们的<a href=\"http://d.163.com/17/0220/10/CDNAR32700314PT8.html\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">2.5.0补丁专题</a>查看，以下是该补丁的改动列表。</p><p><br/></p>', '2017-03-23 17:11:48', '2017-04-18 09:17:25', '', null, '', '新增文章', null, '');
INSERT INTO `sys_article` VALUES ('9', '', '', '29', '新闻中心', '<p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"暗黑3,暴打暗黑,雪暴君\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/201503/47.jpg\"/><br/></p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\"><a href=\"http://play.163.com/special/100w/\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">爱玩网百万稿费</a>活动投稿，作者 雪暴君（特约撰稿人），未经授权请勿转载！</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">书接上回</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\"><a href=\"http://d.163.com/17/0311/10/CF8958H400314U9V.html\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">故纸堆里的暗黑开发故事：做游戏少不了钱和人</a></p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">秃鹫们打算开始制作自己的游戏了，实际上他们一共做了三套方案，第一个叫做《暴君的<a href=\"http://ka.play.163.com/ku/zhengfu/\" title=\"征服\" target=\"_blank\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">征服</a>》，这是一款主题为未来世界的动作游戏，包含偷袭外星人太空站窃取技术以及太空飞船射击等等。第二个叫《考克索水晶》，这是一款阿兹特克主题的射击游戏，玩家们需要探索随机生成的寺庙，去寻找六个魔法水晶。剩下的一个自然就是PC上的大菠萝了。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/327.jpg\"/><br/>暗黑破坏神开发规划</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">前两款游戏的规划颇具野心，他们计划要在世嘉土星, Atari Jaguar以及3DO的新主机上推出。这对于刚刚起步的秃鹫来说，无疑是个巨大的挑战。当时超任平台和世嘉MD都已到了生命周期的尾声，但次时代主机大战的赢家还是没有一点头绪，小小的秃鹫工作室一旦压错宝很有可能就会一蹶不振。到最后Dave 说服了他的朋友们，与其在主机大战中举棋不定，不如挑选一个稳定的不会过时的平台，于是为PC打造的暗黑破坏神计划得以通过。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/328.jpg\"/><br/>幽浮</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">这时候，一款游戏的横空出世引爆了市场，幽浮（X-COM）于1994年的3月推出，这款讲述精英国际组织负责抵御外星人入侵的游戏以其精妙的构思设定鲜明的游戏机制和良好可玩性风靡市场，秃鹫工作室也不例外，尤其是喜欢设定的Erich Schaefer更是对其爱不释手。在幽浮的影响下，暗黑破坏神的设计规划也慢慢成型。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">回合制，倾斜的镜头视角，随机生成的战场和物品等三大元素成为了暗黑初版设计稿的核心。让玩家和怪物轮流行动，玩家必须拥有一个良好的策略在自己的行动回合干掉那些该死的骷髅和骷髅弓箭手。同时与一般的roguelike游戏不同，暗黑将会采用全彩色图形显示。 让秃鹫最花功夫的则是确定镜头的视角，为了同时看清英雄角色，怪物和周边的环境，他们尝试了很多方案，最后选择了今天大家看到的经典斜45°角。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/329.jpg\"/><br/>暗黑关卡设计完全仿造了幽浮的做法</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">但是最核心的系统，他们称之为“动态随机关卡生成”（DRLG），这个思路也是直接来自于幽浮，甚至事实上暗黑所使用的关卡模块的大小与幽浮是一模一样的，他们是直接从幽浮的截图里截取的。在秃鹫设计的地下城关卡里，除了一些大型的“预设”关卡区域之外，所有的房间，拐角，陷阱，宝物，怪物乃至楼梯都是随机生成的，以保障玩家每次进入游戏都会有不同的游戏体验。在这八页纸的开发规划最后，秃鹫写上了自己的需求：一年的开发周期，一个设计师，三个程序员，两个美工，三个画师以及一个音效师。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/330.jpg\"/><br/>黏土动画</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/331.jpg\"/><br/>暗黑开发文档</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">当然这个时候的开发文档考虑到的东西与实际开发还是有点距离的，回合制，单人游戏，DOS系统，类似万智牌那样的后续资料包，黏土动画美术造型以及永久死亡等等设定与暗黑最后推出时的模样大相径庭。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/332.jpg\"/><br/>1994年CES</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">时间来到了1994年的6月，完成了暗黑破坏神设计文案的秃鹫三人组兴致勃勃地来到了芝加哥参加当年的夏季消费电子展（CES），除了把自己开发的《正义联盟特遣部队》在大会上展示并观看把玩其他开发商展出的一款款最新游戏作品之外，他们最重要的任务就是为暗黑破坏神项目找到一家出版商投资。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">迎接他们的是兜头一盆冷水。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">几乎所有的出版商在听到他们规划的暗黑破坏神是一款RPG游戏后，都连忙将他们赶出了展台，仿佛这三个字就是一个会唤来恶魔的咒语。所有的人都对秃鹫三人组重复着一句话：“RPG已经死了，我们不会去投资一款RPG游戏。”即使时间过去了二十多年，David Brevik依然记得他们一共找了25家出版商，没有一个愿意为暗黑破坏神这款RPG游戏进行投资。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/333.jpg\"/><br/>浩劫残阳：破碎之地</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">对于九十年代初期，PC平台上的RPG游戏所面临的困境，很多人都有不同的看法，但总的来说，在CD-ROM，显卡，3D等技术蓬勃发展的时候，那时的传统RPG游戏依然恪守来自龙与地下城里的那一套，没有惊艳的图像表现，没有创新的游戏玩法，转而更注重提供大段大段的故事情节描述，让普通玩家望而却步。这些都让传统RPG在诸如《神秘岛》《毁灭战士》等游戏面前相形见绌。而当时的RPG霸主Strategic Simulations, Inc. (SSI)的做法更是火上浇油，他们推出的《浩劫残阳：破碎之地》等作品平庸的图形，糟糕的动画，各种拼写错误和bug让RPG的粉丝们都大呼受不了，更糟糕的是他们滥发龙与地下城的授权，使得市场上充斥着打着龙与地下城旗号的各种粗制滥造之作。一时间RPG游戏市场上颇有点当年雅达利神话破碎时的意思，各路出版商自然是闻“RPG”而色变，初出茅庐的秃鹫毫无名气，自然也没能支持他们进行更深入的探讨，吃个闭门羹也是很自然不过的事情了。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">随着那些出版商们的一次次拒绝，David Brevik的怒气也仿佛在游戏中一般在不断上涨。秃鹫的这些富有激情的年轻人终于看出了一些端倪 ：这些个出版商们根本不是玩家，他们只是西装革履的生意人。在他们笔挺西装包裹着的身躯里，没有与恶魔奋战到底的欲望，没有用魔法击碎亡灵的激情，没有看到一件神秘新装备的兴奋，没有在艰苦战斗胜利后挥舞的拳头。他们眼中看到的只是数字和报表 - 项目成本，开发规划以及财务预期。这种眼里只有金钱的商人与骨子里还是玩家的开发者之间的分歧一直持续到后来的<a href=\"http://play.163.com/news/\" title=\"暴雪\" target=\"_blank\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">暴雪</a>北方彻底关门大吉。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">在近乎<a href=\"http://ka.play.163.com/shouyou/apple/16781/\" title=\"撞大运\" target=\"_blank\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">撞大运</a>式的挨个敲门展示产品的过程中，秃鹫三人组得到的最友善的回答还是来自老朋友DTMC的开发副总Matt Householder。不过他给出的回应更令人伤感：“Max给我展示了一款名为暗黑破坏神的游戏。 我就跟Max说，游戏听起来是不错，但是DTMC很快就会关门大吉了，所以秃鹫要另找婆家了。”</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/334.jpg\"/><br/>两个版本的正义联盟特遣部队</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">故事讲到这里就要再次用上“山重水复疑无路，柳暗花明又一村”的套路了。秃鹫三人组的“又一村”来自之前他们“自家”的展台，在抱着“就算暗黑推销不出去看一眼自己做的游戏也挺好”的心情又一次经过Sunsoft的展台时，他们看到了令自己震惊的一幕 ——展台上正在演示的确实是正义联盟特遣部队（Justice League Task Force），但却不是他们制作的版本。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">他们制作的是世嘉MD的版本，而在台上演示的却是超任版。直到这个时候他们才知道还有另一家公司在为同一个名字的游戏在制作另一个平台上的版本。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/335.jpg\"/><br/>硅与神经键</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">负责制作超任版的工作室叫着一个很古怪的名字——硅与神经键（Silicon &amp; Synapse）。这名字取的如此稀奇古怪，人们常常会把“硅”（silicon）拼成了“硅胶”（silicone），于是这家游戏工作室常常会被误认为是一家丰胸整形公司。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">更让他们惊讶的是，尽管在此之前两个工作室相互不认识，尽管他们工作室的位置相距数百英里，尽管他们是在为不同的硬件平台开发，但是两个版本的游戏玩起来居然惊人的相似（当然都是街霸2的仿作嘛）。时至今日，我们在网上依然可以找到两个版本的正义联盟特遣部队游戏同步对比视频，从人物招式到游戏进程确实有非常多相似的地方。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/336.jpg\"/><br/>两个版本的正义联盟特遣部队对比</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">这样的巧合让秃鹫三人组产生了莫大的兴趣，他们与硅与神经键工作室的小伙子们攀谈了起来。一开始他们之间还存着一份较量的心思，毕竟他们都是在做同一款游戏嘛，比较一番是很自然的，但是很快他们就有了共同的话题：游戏和制作自己的游戏。秃鹫三人组在硅与神经键的小伙子们身上看到了同为玩家身份的共鸣，那就是对游戏的热情。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/348.jpg\" width=\"600\"/><br/>魔兽争霸</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">他们很快无话不谈，秃鹫给硅与神经键展示了自己的PC游戏暗黑破坏神的规划，而硅与神经键则神神秘秘地将秃鹫请到了一间阴暗狭窄的展示间（事实上就是在展台后面用隔板隔出来的）给他们演示自己的PC平台野心之作 - 魔兽争霸。这款受即时战略游戏鼻祖沙丘2启发而制作的人兽大战作品就是硅与神经键工作室谋划已久准备推向市场的重磅炸弹。David Brevik在试玩之后当即表示：“你们还缺beta测试者么？我这儿有一整个公司的人可以帮你们来测试啊。”硅与神经键则很有礼貌地回复说“好的，在我们做完魔兽争霸之后，我们会来听听你们的项目规划。”</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">那么同为小型游戏工作室，秃鹫在为生存苦苦挣扎，而硅与神经键是哪儿来的大笔资金得以打造自己的PC游戏呢？</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">相对秃鹫的磕磕碰碰，硅与神经键的成长历程则算的上是比较顺风顺水。它的核心人物是Allen Adham。跟David Brevik一样，Allen Adham也是打小接触游戏，在高中时已是小有名气的编程高手了，他利用假期打工给商业公司写代码一个暑假就能入账一万两千美元，这可是上世纪的一万多美刀啊。高中毕业后Allen先去参军，退伍之后来到加利福尼亚大学洛杉矶分校进修计算机科学。有一天Allen在机房猛敲代码，觉得有点渴了起身去喝水，随手把自己用的那台机锁定了，回来之后他大吃一惊，原来的位置上已经有人了，而且解开了自己用密码锁定的计算机，原来他们使用了同一个密码“JOE”，这个人就是Mike Morhaime。随后他又结识了另一个来自UCLA的学生Frank Pearce。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">1990年Mike Morhaime和Frank Pearce毕业了，Mike去西数做硬盘去了，Frank则来到了一家制造业公司。他们的生活似乎就要走向安逸稳定波澜不兴，是稍后毕业的Allen打破了这一切，他热情地邀请两位程序员加入他成立一家工作室去制作游戏。面对两人的犹豫不决，Allen用一句话打动了他们 - “我们还年轻，我们还能拼。”</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/349.jpg\"/><br/>Pat Wyatt</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">于是1991年的2月，硅与神经键工作室成立了。Mike从奶奶那里借来了一万五千美元作为启动资金，Allen也掏出了自己的全部家当。他们的第一份合同来自Interplay，其老板Brian Fargo是Allen Adham的好朋友。跟秃鹫一样，他们的起步也是从移植游戏开始的，第一个就是将DOS下的西洋战棋移植到Windows上，这让工作室引入了一名新程序员Pat Wyatt，请记住这位大大的名字，因为之后暴雪前WOW时代几乎所有的游戏编程都与他有关，之后他离开工作室成立了ArenaNet推出了《激战》等大作。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">成立初期硅与神经键工作室几乎什么活儿都接，只为尽量让工作室成长起来让生涩的大学生变身为熟练的程序员，不过他们依然只能勉强维持，Mike和Allen甚至多次刷信用卡来支付员工薪水，近三年时间里他们的私人帐户里没有一分钱入账。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/339.jpg\"/><br/>RPM赛车</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">资金短缺的转机还是来自Interplay的老板Brian Fargo，他为硅与神经键带来了一份与EA签订的出版合同，将Commodore 64上的《Racing Destruction Set》重制移植到任<a href=\"http://ka.play.163.com/ku/ineage/\" title=\"天堂\" target=\"_blank\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">天堂</a>的最新主机上，于是为硅与神经键制作的《RPM赛车》成为了首款由美国公司制作的登上超任平台的游戏。Brian Fargo对硅与神经键的工作成果十分满意，提出可以让Interplay来出版这家工作室自主开发的游戏，这就是后来的《失落的维京人》以及RPM赛车的续作《摇滚赛车》，这让硅与神经键一下子“财大气粗”起来，因为之前他们的移植合同大多是3个月开发1万美元收入，而摇滚赛车让他们拿到了8个月开发4万美元的收入。摇滚赛车还拿到了电子游戏月刊1993年的最佳赛车游戏奖项。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/350.jpg\" width=\"600\"/><br/>失落的维京人</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/351.jpg\"/><br/>摇滚赛车</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">凭借两个原创游戏打下的名气，硅与神经键成为《电子游戏杂志》评出的1993年年度最佳软件开发商。这无疑让年轻的工作室士气大振，但Allen Adham依然不太满意，一方面是Interplay出资出版自然拿走了大部分的利润，一方面Interplay方面糟糕的市场营销方案大大影响了了游戏的销量，他举出了一个例子——一款摇滚赛车游戏居然用上了粉红色的包装。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">业界风云变幻总是很有意思的，最初是从雅达利独立出来的动视扶持起了Brian Fargo组建的Interplay，后者在暴雪草创期间出力甚多，最后动视和暴雪组成了新的巨无霸。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">打响名气之后，更多的工作找上门来，Allen Adham越来越多地进入到老板的角色中，各种谈判合同签署接收项目。在这期间，有两个项目合同的签订最为关键，一个是在1993年的夏天，来自Sunsoft的出版总监David Siller与工作室签订了超任版的正义联盟特遣部队开发协议，仅仅几天前这位总监刚与秃鹫三人组签署了世嘉版的制作合同，为两家工作室的命运相会打下了基础。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/342.jpg\"/><br/>数学大爆破</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">另一份合同则是为一家名为Davidson &amp; Associates的教育软件公司制作一款打字训练游戏。Davidson公司刚推出了自己的王牌产品《数学大爆破》,那是一款“寓教于乐”型的游戏，市场反应热烈。于是Davidson公司立马开始了大规模的扩张以巩固自己的软件<a href=\"http://ka.play.163.com/shouyou/apple/15079/\" title=\"帝国\" target=\"_blank\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">帝国</a>，他们陆续进行了多次战略收购，同时也看上了硅与神经键这家锋芒初露的工作室，意图让硅与神经键来制作“纯游戏”软件作为自己软件帝国的一个补充。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">但是硅与神经键的三位创始人却是不太情愿，在多次商谈之后，他们表达了自己的担心：因为自成立以来硅与神经键多是在进行“合同式”的移植游戏工作，即使是原创作品也是在出版商的把控之下，他们想做自己的原创游戏，不愿意受到太多的约束。于是他们把工作室的保价抬到了一个自认为夸张的地步（还记得Mike当初投资工作室的启动资金是多少么？），想以此回绝Davidson公司的收购意愿。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">没想到的是，Davidson公司一口答应了他们的报价，同时保证游戏开发相关事宜都由硅与神经键工作室自己做主，Davidson绝不干涉。于是在1994年的2月，硅与神经键工作室作价六百七十五万美元纳入到Davidson公司的版图中。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">一夜之间，硅与神经键的三位创始人成为了百万富翁，背靠Davidson公司强大的营销网络，他们的游戏现在可以更便捷更有目的性地投放到市场上了。不过纳入Davidson公司版图后他们要做的第一件事情不是扩充人手，而是——改名。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/343.jpg\"/><br/>混沌工作室以及年少的Mike Morhaime</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">在极具市场眼光的Davidson公司看来，硅与神经键这个名字实在没法用，人们甚至不能完整正确地拼出来。于是他们先改名为混沌工作室，后来发现这名字已经被占用了只能继续改。Allen Adham提出叫食人魔工作室，也因与教育软件公司形象实在不搭而被否决，最后是Allen去翻字典找到了沿用至今的名字——暴雪。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/344.jpg\"/><br/>Bill Roper</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">取名风波尘埃落定之后，暴雪开始了扩张，工作室的规模扩充到了15人，一些我们耳熟能详的名字比如 Bill Roper，Samwise Didier等就是在这个时候加入暴雪的。</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/345.jpg\"/><br/>Samwise Didier</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">兵强马壮粮草充足，暴雪开始了自己的魔兽争霸计划。他们瞄准的是沙丘2引爆的即时战略游戏市场。他们把游戏的核心设定为人兽大战，Samwise Didier做出了大量人族和兽族的原画设定。Bill Roper则是一位多面手，从抓bug到游戏配音他什么活儿都要干，尤其是他的角色配音惟妙惟肖，让不同种族的声音形象一下子鲜活起来。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">尽管是仿沙丘2的作品，暴雪还是做出了自己的改动，他们瞄准沙丘2缺乏多人游戏的缺点，主打联机对战功能，Pat Wyatt作为主力程序员担负起了这个艰巨的任务。在无数的熬夜抓bug之后，魔兽争霸的多人游戏功能终于成型。Allen就是带着这份演示版踏上了前往CES消费电子展的路程。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">总而言之，两家工作室在芝加哥会面了，此时的暴雪正在加班加点地制作着自己的魔兽争霸，Davidson公司正计划着将这款游戏摆上圣诞节的货架。而秃鹫则四处推销着自己的暗黑破坏神计划，寻找着一家肯出资的出版商。来自暴雪礼貌性的一句“等我们做完魔兽有时间再来看看”并没有让秃鹫的人安下心来，有更严峻的考验在等着他们。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">那段时间秃鹫的主要资金来源Sunsoft遇到了大麻烦，这家总部位于日本爱知县的公司在超任时代的末期接连遭受打击，使得其在美国和欧洲的工作室也被迫关闭了。而到了1995年年初， 它甚至面对破产重组的窘境，旗下所有的未发布项目（当然也包括正义联盟特遣部队）及相关权益都被出售给了Acclaim Entertainment或者干脆就取消了。这里面最主要的原因则是其母公司Sun电子在加州旅游城市棕榈泉建设高尔夫球场的投资大失败损失了大笔资金。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">事发突然，秃鹫和暴雪都没有从Sunsoft听到一点消息，暴雪这边有Davidson公司养着情况还好，而秃鹫的处境就不太秒了。随着Sunsoft和Acclaim之间相互扯皮，应许的项目开发资金迟迟不到位，秃鹫陷入了巨大的困境，他们开始向亲戚朋友借钱度日，心里想着却还是继续游戏开发。这些对商业运作几乎一无所知的人们，他们唯一需要担心的是 - 要怎么缴税？Erich Schaefer回忆说：“我们没有碰上特别大的麻烦，但这时候税务官开始上门了，他说：“嘿，钱在哪儿？”我们就乱作一团。”</p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><img alt=\"故纸堆里的暗黑开发故事：暗黑破坏神开发启动\" src=\"http://img1.cache.netease.com/game/diablo3/d3/news/2017003/346.jpg\"/><br/>魔兽争霸</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">魔兽争霸在1994年11月上市并颇受玩家欢迎，暴雪在PC上的自主游戏一炮打响，Allen也想乘热打铁多找几个项目，正巧这时陷入窘境的Dave打来电话，一方面感谢他们在CES上的招待，另一方面也探探暴雪的口风，看看他们在魔兽之后，是否依然还对暗黑项目抱有兴趣。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">双方一拍即合，于是1995年的正月，Allen Adham 和 Pat Wyatt 就驱车来到了红木市秃鹫的办公场地，秃鹫三人组给他们展示了初始的暗黑规划，回合制，随机生成，单人游戏等等。整个演示甚至都不到一个小时，但暴雪的人很快就接受了，因为这不是一场开发者对出版商的演示，而是一次玩家与玩家之间的交流，他们拥有着相同的玩家基因，他们对玩家的游戏体验感同身受，于是在这短短的会面之后他们就达成了共识，暴雪将会出资帮助秃鹫制作这款原创的PC游戏。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">而暴雪的条件只有两个：第一，将回合制改为即时制；第二，将单人游戏改为多人游戏。这是暴雪从魔兽争霸成功身上获得宝贵经验，现在这个经验也要用到暗黑身上了。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">1995年2月，暴雪与秃鹫正式签署合同，暴雪出资30万美元支持暗黑项目。至此，暗黑破坏神的开发正式启动。</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\">未完待续……</p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\"><strong><a href=\"http://play.163.com/special/100w/\" style=\"outline: 0px; text-decoration: none; color: rgb(255, 255, 255) !important;\">欢迎参加——爱玩网百万稿费活动：当金牌作者，开网易专栏，领丰厚稿费，得专属周边！</a></strong></p><p style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; text-indent: 2em; font-size: 16px; line-height: 28px;\"><strong><a href=\"http://play.163.com/special/100w/\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\">详情请看：</a></strong></p><p class=\"f_center\" style=\"margin-top: 18px; margin-bottom: 18px; padding: 0px; border: 0px; outline: 0px; word-break: break-all; word-wrap: break-word; font-size: 16px; line-height: 28px; text-align: center;\"><a href=\"http://play.163.com/special/100w/\" style=\"outline: none; text-decoration: none; color: rgb(164, 96, 30);\"><img alt=\"爱玩网百万稿费活动\" src=\"http://img5.cache.netease.com/game/2016/4/11/201604111528227b5ab.jpg\"/></a></p><p><a href=\"http://play.163.com/\" class=\"icon\" style=\"outline: none; text-decoration: none; color: rgb(185, 157, 110); width: 13px; height: 12px; display: inline-block; vertical-align: middle; background: url(&quot;http://img1.cache.netease.com/cnews/css13/img/end_game.png&quot;) 0px 50% no-repeat;\"></a>&nbsp;<span style=\"margin: 0px; padding: 0px 5px; border: 0px; outline: 0px;\">本文来源：<a href=\"http://d.163.com/\" style=\"outline: none; text-decoration: none; color: rgb(185, 157, 110);\">凯恩之角</a></span>&nbsp;<span style=\"margin: 0px; padding: 0px 5px; border: 0px; outline: 0px;\">责任编辑：吕晟辰_NG6316</span></p><p><br/></p>', '2017-03-23 17:11:50', '2017-03-23 17:10:46', '', null, '', '测试文章测试文章测试文章测试文章测试文章测试文章', null, 'http://om6onp8sp.bkt.clouddn.com/d130b3d8-0d8f-45be-a04b-0ab1df9b1f9d.jpg');
INSERT INTO `sys_article` VALUES ('10', '', '', '30', '关于我们', '<p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">　　中兴通讯是全球领先的综合通信解决方案提供商。公司通过为全球160多个国家和地区的电信运营商和企业网客户提供创新技术与产品解决方案，让全世界用户享有语音、数据、多媒体、无线宽带等全方位沟通。公司成立于1985年，在香港和深圳两地上市，是中国最大的通信设备上市公司。</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">&nbsp;</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">　　中兴通讯拥有通信业界最完整的、端到端的产品线和融合解决方案，通过全系列的无线、有线、业务、终端产品和专业通信服务，灵活满足全球不同运营商和企业网客户的差异化需求以及快速创新的追求。2014年中兴通讯实现营业收入814.7亿元人民币，净利润26.3亿元人民币，同比增长94%。目前，中兴通讯已全面服务于全球主流运营商及企业网客户，智能终端发货量位居美国前四，并被誉为“智慧城市的标杆企业”。</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">&nbsp;</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">　　中兴通讯坚持以持续技术创新为客户不断创造价值。公司在美国、法国、瑞典、印度、中国等地共设有20个全球研发机构，近3万名国内外研发人员专注于行业技术创新；PCT专利申请量近5年均居全球前三，2011、2012年PCT蝉联全球前一。公司依托分布于全球的107个分支机构，凭借不断增强的创新能力、突出的灵活定制能力、日趋完善的交付能力赢得全球客户的信任与合作。</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">&nbsp;</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">　　中兴通讯为联合国全球契约组织成员，坚持在全球范围内贯彻可持续发展理念，实现社会、环境及利益相关者的和谐共生。我们运用通信技术帮助不同地区的人们享有平等的通信自由；我们将“创新、融合、绿色”理念贯穿到整个产品生命周期，以及研发、生产、物流、客户服务等全流程，为实现全球性降低能耗和二氧化碳排放不懈努力。我们还在全球范围内开展社区公益和救助行动，参加了印尼海啸、海地及汶川地震等重大自然灾害救助，并成立了中国规模最大的“关爱儿童专项基金”。</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">&nbsp;</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal;\">　　未来，中兴通讯将继续致力于引领全球通信产业的发展，应对全球通信领域更趋日新月异的挑战。　</p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; color: rgb(88, 88, 88); font-family: Arial, Helvetica, sans-serif, 宋体; font-size: 14px; line-height: 25px; white-space: normal; text-align: center;\"><a href=\"http://www.zte.com.cn/cnvideo/press_center/video/201402/t20140213_417910.html\" style=\"text-decoration: none; color: rgb(88, 88, 88); outline: none;\"><img src=\"http://www.zte.com.cn/cn/about/corporate_information/201410/W020141028391057404249.jpg\" width=\"600\" height=\"371\"/></a></p><p><br/></p>', '2017-03-24 10:47:49', '2017-03-24 10:58:28', '中兴通讯是全球领先的综合通信解决方案提供商。公司通过为全球160多个国家和地区的电信运营商和企业网客户提供创新技术与产品解决方案，让全世界用户享有语音、数据、多媒体、无线宽带等全方位沟通。公司成立于1985年，在香港和深圳两地上市，是中国最大的通信设备上市公司。', null, '', '公司简介', null, '');
INSERT INTO `sys_article` VALUES ('12', '', '', '32', '联系我们', '<p style=\"text-align: center;\"><img width=\"530\" height=\"340\" src=\"http://api.map.baidu.com/staticimage?center=120.780885,31.493506&zoom=10&width=530&height=340&markers=116.404,39.915\"/><br/></p>', '2017-04-18 18:05:11', null, '', null, '', '联系我们', null, '');

-- ----------------------------
-- Table structure for sys_attachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_attachment`;
CREATE TABLE `sys_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_attachment
-- ----------------------------
INSERT INTO `sys_attachment` VALUES ('134', '2017-03-02 18:42:13', 'image001.jpg', '40.8877', 'image/jpeg', 'http://kd.cache.timepack.cn/a0c3635e-d10c-45fa-b337-6d36235b225f.jpg');
INSERT INTO `sys_attachment` VALUES ('135', '2017-03-02 18:53:39', '2016120922234214704.jpg', '70.9551', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/12f0719d-a7a5-48b1-a7dd-f72f61ba2cda.jpg');
INSERT INTO `sys_attachment` VALUES ('136', '2017-03-03 10:22:42', '2016120922234214704.jpg', '70.9551', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/0b797c3d-3192-495d-8b1c-6641615212cc.jpg');
INSERT INTO `sys_attachment` VALUES ('137', '2017-03-03 10:22:42', '2015100110335745690.jpg', '193.142', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/0aa296dc-6c8f-4550-ab9b-2a5ebe009838.jpg');
INSERT INTO `sys_attachment` VALUES ('138', '2017-03-03 10:22:42', 'HGCmsUpFlie_92017022809382291.jpg', '53.5137', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/99750a36-0f0b-45a6-888c-d6ee5c1f393e.jpg');
INSERT INTO `sys_attachment` VALUES ('139', '2017-03-03 10:22:42', 'image001.jpg', '40.8877', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/49c0656c-c551-414e-b45f-9be8f86a5d47.jpg');
INSERT INTO `sys_attachment` VALUES ('140', '2017-03-03 10:22:43', '2015100110335252040.jpg', '527.111', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/85f074ca-aa1d-4168-81f2-9c16f026fb52.jpg');
INSERT INTO `sys_attachment` VALUES ('141', '2017-03-03 10:26:23', '37203249_p0_master1200.jpg', '1048.6', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/46a3c212-f373-4c21-bdd4-a0d95acc91e8.jpg');
INSERT INTO `sys_attachment` VALUES ('142', '2017-03-03 10:26:37', '34512986_p0_master1200.jpg', '376.337', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/2227313d-44b3-4e42-950f-c1bc17db6919.jpg');
INSERT INTO `sys_attachment` VALUES ('144', '2017-03-03 10:28:24', '26339586_p0_master1200.jpg', '751.91', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/13f82457-5fc0-4d71-b99b-cf7f21b16988.jpg');
INSERT INTO `sys_attachment` VALUES ('145', '2017-03-03 10:28:38', '57196809_p0_master1200.jpg', '519.826', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/160ab4e6-c195-424f-aa31-b646bcce9b94.jpg');
INSERT INTO `sys_attachment` VALUES ('146', '2017-03-03 10:28:47', '34512986_p0_master1200.jpg', '376.337', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/c2b78b21-1094-4c8c-9162-8a77e0b54ef4.jpg');
INSERT INTO `sys_attachment` VALUES ('147', '2017-03-10 17:51:33', 'image001.jpg', '40.8877', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/42f2ce74-af13-4deb-a0ef-620324c2551a.jpg');
INSERT INTO `sys_attachment` VALUES ('148', '2017-03-14 14:30:05', '2016120922234214704.jpg', '70.9551', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/bb0fb347-c1e4-4ce6-83d6-0ae862c2707f.jpg');
INSERT INTO `sys_attachment` VALUES ('149', '2017-03-14 14:30:05', '2015100110335745690.jpg', '193.142', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/3d0c5bab-49bf-4bae-90f7-50fb9afdefee.jpg');
INSERT INTO `sys_attachment` VALUES ('150', '2017-03-14 14:30:05', 'image001.jpg', '40.8877', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/34419896-1d1b-43a8-b280-41fcf9d245f4.jpg');
INSERT INTO `sys_attachment` VALUES ('151', '2017-03-14 14:30:05', 'HGCmsUpFlie_92017022809382291.jpg', '53.5137', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/8935be0c-f65e-436d-bb95-35aedc1aa752.jpg');
INSERT INTO `sys_attachment` VALUES ('152', '2017-03-14 14:30:05', '2015100110335252040.jpg', '386.08', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/7fe5028a-4532-4dc9-9b8a-828c61255575.jpg');
INSERT INTO `sys_attachment` VALUES ('153', '2017-03-14 15:56:07', 'HGCmsUpFlie_92017022809382291.jpg', '53.5137', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/5d041119-d186-467b-a804-c16db4b911bf.jpg');
INSERT INTO `sys_attachment` VALUES ('154', '2017-03-14 16:34:05', '2015100110335745690.jpg', '193.142', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/e8eb3366-a99a-4732-9e4e-3531c4179296.jpg');
INSERT INTO `sys_attachment` VALUES ('155', '2017-03-14 17:52:34', 'NKFR_4CU28~L2KN9YZ2@R0O.gif', '2205.62', 'image/gif', 'http://om6onp8sp.bkt.clouddn.com/c43c41f1-98f9-425e-9882-d863eebb0415.gif');
INSERT INTO `sys_attachment` VALUES ('156', '2017-03-16 14:26:34', 'testMp.txt', '0.00878906', 'text/plain', 'http://om6onp8sp.bkt.clouddn.com/4783aaa2-337d-418e-83f6-29c9ff06e666.txt');
INSERT INTO `sys_attachment` VALUES ('157', '2017-03-21 11:20:04', 'slide_simple_bg.jpg', '271.583', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/e25f6141-e09e-48ab-bfec-a70bd48240a5.jpg');
INSERT INTO `sys_attachment` VALUES ('158', '2017-03-22 14:29:02', 'b01.jpg', '13.999', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/fc8b07df-2fa3-4e15-9f0d-ffd6fa8ca2e3.jpg');
INSERT INTO `sys_attachment` VALUES ('159', '2017-03-22 14:30:28', 'post01.jpg', '35.0576', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/9224c99f-beed-4061-98dd-0c7b0c0f35eb.jpg');
INSERT INTO `sys_attachment` VALUES ('160', '2017-03-22 14:30:48', 'post02.jpg', '18.1924', 'image/jpeg', 'http://om6onp8sp.bkt.clouddn.com/d130b3d8-0d8f-45be-a04b-0ab1df9b1f9d.jpg');
INSERT INTO `sys_attachment` VALUES ('161', '2017-03-24 10:58:04', 'company.png', '114.306', 'image/png', 'http://om6onp8sp.bkt.clouddn.com/f2a75559-4a9a-477c-b401-7adbe9c62cad.png');

-- ----------------------------
-- Table structure for sys_category
-- ----------------------------
DROP TABLE IF EXISTS `sys_category`;
CREATE TABLE `sys_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `en_name` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `parent_ids` varchar(255) DEFAULT NULL,
  `html_url` varchar(255) DEFAULT NULL,
  `list_template` varchar(255) DEFAULT NULL,
  `content_template` varchar(255) DEFAULT NULL,
  `parent_name` varchar(255) DEFAULT NULL,
  `show_order` int(11) DEFAULT NULL,
  `list_type` varchar(255) DEFAULT NULL,
  `blank` bit(1) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `sp` varchar(255) DEFAULT NULL COMMENT '页面碎片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_category
-- ----------------------------
INSERT INTO `sys_category` VALUES ('25', '', '网站首页', '', '0', null, '/', 'index.ftl', '', '根节点', null, '', '', '', 'banner.ftl,foot.ftl,head.ftl,lanmu.ftl,lxwm.ftl,mobileHeader.ftl');
INSERT INTO `sys_category` VALUES ('26', '', '产品中心', 'cpzx', '0', null, '/cpzx', 'list_image.ftl', 'content.ftl', '根节点', null, '', '', '', '');
INSERT INTO `sys_category` VALUES ('27', '', '客户案例', 'khal', '0', null, '/khal', 'list_image.ftl', 'content.ftl', '根节点', null, '', '', '', '');
INSERT INTO `sys_category` VALUES ('28', '', '解决方案', 'jjfa', '0', null, '/jjfa', 'singlepage.ftl', '', '根节点', null, '', '', '', '');
INSERT INTO `sys_category` VALUES ('29', '', '新闻中心', 'xwzx', '0', null, '/xwzx', 'list_news.ftl', 'content.ftl', '根节点', null, '', '', '', 'sy_news.ftl');
INSERT INTO `sys_category` VALUES ('30', '', '关于我们', 'gywm', '0', null, '/gywm', 'singlepage.ftl', '', '根节点', null, '', '', '', '');
INSERT INTO `sys_category` VALUES ('31', '', '加入我们', 'jrwm', '0', null, '/jrwm', 'singlepage.ftl', 'content.ftl', '根节点', null, '', '', '', '');
INSERT INTO `sys_category` VALUES ('32', '', '联系我们', 'lxwm', '0', null, '/lxwm', 'singlepage.ftl', '', '根节点', null, '', '', '', '');
INSERT INTO `sys_category` VALUES ('33', '', '产品中心1', 'cpzx1', '26', '26', '/cpzx/cpzx1', 'list_image.ftl', 'content.ftl', '产品中心', null, '', '', '', '');

-- ----------------------------
-- Table structure for sys_category_article
-- ----------------------------
DROP TABLE IF EXISTS `sys_category_article`;
CREATE TABLE `sys_category_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_category_article
-- ----------------------------

-- ----------------------------
-- Table structure for sys_friendship_link
-- ----------------------------
DROP TABLE IF EXISTS `sys_friendship_link`;
CREATE TABLE `sys_friendship_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `show_order` int(11) DEFAULT NULL,
  `site_logo` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_friendship_link
-- ----------------------------
INSERT INTO `sys_friendship_link` VALUES ('9', '', '2', 'http://om6onp8sp.bkt.clouddn.com/46a3c212-f373-4c21-bdd4-a0d95acc91e8.jpg', '想222', 'http://www.baidu.com');
INSERT INTO `sys_friendship_link` VALUES ('10', '', '2', 'http://om6onp8sp.bkt.clouddn.com/475a2b51-6923-4a7c-86d5-d493bcd30eab.jpg', '想111', 'http://www.qq.com');

-- ----------------------------
-- Table structure for sys_permissions
-- ----------------------------
DROP TABLE IF EXISTS `sys_permissions`;
CREATE TABLE `sys_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `show_order` varchar(255) DEFAULT NULL,
  `parent_name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permissions
-- ----------------------------
INSERT INTO `sys_permissions` VALUES ('1', '', '根节点', 'root', '0', null, null, null);
INSERT INTO `sys_permissions` VALUES ('2', '', '首页', 'admin/index', '1', '1', '根节点', 'am-icon-home');
INSERT INTO `sys_permissions` VALUES ('3', '', '系统管理', 'admin', '1', '2', '根节点', 'am-icon-calendar');
INSERT INTO `sys_permissions` VALUES ('4', '', '菜单管理', 'menu/list', '3', '4', '系统管理', null);
INSERT INTO `sys_permissions` VALUES ('6', '', '用户管理', 'user/list', '3', '4', '系统管理', 'am-icon-github');
INSERT INTO `sys_permissions` VALUES ('43', '', '角色管理', 'role/list', '3', '2', '系统管理', 'am-icon-drupal');
INSERT INTO `sys_permissions` VALUES ('49', '', '菜单新增', 'menu/add', '4', '1', '菜单管理', '');
INSERT INTO `sys_permissions` VALUES ('50', '', '菜单修改', 'menu/edit', '4', '5', '菜单管理', '');
INSERT INTO `sys_permissions` VALUES ('51', '', '菜单删除', 'menu/del', '4', null, '菜单管理', '');
INSERT INTO `sys_permissions` VALUES ('52', '', '内容管理', 'cms', '1', '2', '根节点', 'am-icon-th');
INSERT INTO `sys_permissions` VALUES ('53', '', '栏目管理', 'category/list', '52', '1', '内容管理', '');
INSERT INTO `sys_permissions` VALUES ('54', '', '栏目新增', 'category/add', '53', '1', '栏目管理', '');
INSERT INTO `sys_permissions` VALUES ('55', '', '栏目修改', 'category/edit', '53', '2', '栏目管理', '');
INSERT INTO `sys_permissions` VALUES ('56', '', '栏目删除', 'category/del', '53', '3', '栏目管理', '');
INSERT INTO `sys_permissions` VALUES ('57', '', '文章管理', 'article/list', '52', '2', '内容管理', '');
INSERT INTO `sys_permissions` VALUES ('58', '', '文章新增', 'article/add', '57', '1', '文章管理', '');
INSERT INTO `sys_permissions` VALUES ('59', '', '文章修改', 'article/edit', '57', '2', '文章管理', '');
INSERT INTO `sys_permissions` VALUES ('60', '', '文章删除', 'article/del', '57', '3', '文章管理', '');
INSERT INTO `sys_permissions` VALUES ('61', '', '附件管理', 'attachment/list', '52', '3', '内容管理', 'am-icon-file');
INSERT INTO `sys_permissions` VALUES ('62', '', '附件新增', 'attachment/add', '61', '1', '附件管理', '');
INSERT INTO `sys_permissions` VALUES ('63', '', '附件修改', 'attachment/edit', '61', '2', '附件管理', '');
INSERT INTO `sys_permissions` VALUES ('64', '', '附件删除', 'attachment/del', '61', '3', '附件管理', '');
INSERT INTO `sys_permissions` VALUES ('65', '', '用户新增', 'user/add', '6', '1', '用户管理', 'am-icon-drupal');
INSERT INTO `sys_permissions` VALUES ('66', '', '用户修改', 'user/edit', '6', '2', '用户管理', '');
INSERT INTO `sys_permissions` VALUES ('67', '', '用户删除', 'user/del', '6', '3', '用户管理', '');
INSERT INTO `sys_permissions` VALUES ('68', '', '系统设置', 'system', '1', '9', '根节点', 'am-icon-shield');
INSERT INTO `sys_permissions` VALUES ('69', '', '角色新增', 'role/add', '43', '1', '角色管理', '');
INSERT INTO `sys_permissions` VALUES ('70', '', '角色修改', 'role/edit', '43', '2', '角色管理', '');
INSERT INTO `sys_permissions` VALUES ('71', '', '角色删除', 'role/del', '43', '3', '角色管理', '');
INSERT INTO `sys_permissions` VALUES ('72', '', '友情链接', 'friendshipLink/list', '68', '1', '系统设置', '');
INSERT INTO `sys_permissions` VALUES ('73', '', '链接新增', 'friendshipLink/add', '72', '1', '友情链接', '');
INSERT INTO `sys_permissions` VALUES ('74', '', '链接修改', 'friendshipLink/edit', '72', '3', '友情链接', '');
INSERT INTO `sys_permissions` VALUES ('75', '', '链接删除', 'friendshipLink/del', '72', '4', '友情链接', '');
INSERT INTO `sys_permissions` VALUES ('76', '', '站点设置', 'siteInfo/edit', '68', '0', '系统设置', '');
INSERT INTO `sys_permissions` VALUES ('77', '', '幻灯管理', 'slideImg/list', '52', '1', '内容管理', '');
INSERT INTO `sys_permissions` VALUES ('78', '', '幻灯新增', 'slideImg/add', '77', '1', '幻灯管理', '');
INSERT INTO `sys_permissions` VALUES ('79', '', '幻灯修改', 'slideImg/edit', '77', '2', '幻灯管理', '');
INSERT INTO `sys_permissions` VALUES ('80', '', '幻灯删除', 'slideImg/del', '77', '3', '幻灯管理', '');
INSERT INTO `sys_permissions` VALUES ('82', '', '球场管理', 'stadium/list', '52', '5', '内容管理', '');
INSERT INTO `sys_permissions` VALUES ('83', '', '球场新增', 'stadium/add', '82', null, '球场管理', '');
INSERT INTO `sys_permissions` VALUES ('84', '', '球场删除', 'stadium/del', '82', null, '球场管理', '');
INSERT INTO `sys_permissions` VALUES ('85', '', '球场编辑', 'stadium/edit', '82', null, '球场管理', '');
INSERT INTO `sys_permissions` VALUES ('86', '', '生成静态页面', 'category/createAllHtml', '53', '4', '栏目管理', 'am-icon-file');

-- ----------------------------
-- Table structure for sys_roles
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles`;
CREATE TABLE `sys_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `available` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `show_order` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_roles
-- ----------------------------
INSERT INTO `sys_roles` VALUES ('6', '', '超级管理员', 'admin', '');
INSERT INTO `sys_roles` VALUES ('8', '', '普通管理员', 'articleadmin', '');

-- ----------------------------
-- Table structure for sys_roles_permissions
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_permissions`;
CREATE TABLE `sys_roles_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permission_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=474 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_roles_permissions
-- ----------------------------
INSERT INTO `sys_roles_permissions` VALUES ('27', '2', '9');
INSERT INTO `sys_roles_permissions` VALUES ('28', '3', '9');
INSERT INTO `sys_roles_permissions` VALUES ('29', '43', '9');
INSERT INTO `sys_roles_permissions` VALUES ('30', '4', '9');
INSERT INTO `sys_roles_permissions` VALUES ('31', '51', '9');
INSERT INTO `sys_roles_permissions` VALUES ('32', '49', '9');
INSERT INTO `sys_roles_permissions` VALUES ('33', '50', '9');
INSERT INTO `sys_roles_permissions` VALUES ('34', '6', '9');
INSERT INTO `sys_roles_permissions` VALUES ('35', '2', '10');
INSERT INTO `sys_roles_permissions` VALUES ('36', '3', '10');
INSERT INTO `sys_roles_permissions` VALUES ('37', '43', '10');
INSERT INTO `sys_roles_permissions` VALUES ('38', '4', '10');
INSERT INTO `sys_roles_permissions` VALUES ('39', '51', '10');
INSERT INTO `sys_roles_permissions` VALUES ('40', '49', '10');
INSERT INTO `sys_roles_permissions` VALUES ('41', '50', '10');
INSERT INTO `sys_roles_permissions` VALUES ('42', '6', '10');
INSERT INTO `sys_roles_permissions` VALUES ('43', '2', '11');
INSERT INTO `sys_roles_permissions` VALUES ('44', '3', '11');
INSERT INTO `sys_roles_permissions` VALUES ('45', '43', '11');
INSERT INTO `sys_roles_permissions` VALUES ('46', '4', '11');
INSERT INTO `sys_roles_permissions` VALUES ('47', '51', '11');
INSERT INTO `sys_roles_permissions` VALUES ('48', '49', '11');
INSERT INTO `sys_roles_permissions` VALUES ('49', '50', '11');
INSERT INTO `sys_roles_permissions` VALUES ('50', '6', '11');
INSERT INTO `sys_roles_permissions` VALUES ('51', '2', '12');
INSERT INTO `sys_roles_permissions` VALUES ('52', '3', '12');
INSERT INTO `sys_roles_permissions` VALUES ('53', '43', '12');
INSERT INTO `sys_roles_permissions` VALUES ('54', '4', '12');
INSERT INTO `sys_roles_permissions` VALUES ('55', '51', '12');
INSERT INTO `sys_roles_permissions` VALUES ('56', '49', '12');
INSERT INTO `sys_roles_permissions` VALUES ('57', '50', '12');
INSERT INTO `sys_roles_permissions` VALUES ('58', '6', '12');
INSERT INTO `sys_roles_permissions` VALUES ('59', '2', '13');
INSERT INTO `sys_roles_permissions` VALUES ('60', '3', '13');
INSERT INTO `sys_roles_permissions` VALUES ('61', '43', '13');
INSERT INTO `sys_roles_permissions` VALUES ('62', '4', '13');
INSERT INTO `sys_roles_permissions` VALUES ('63', '51', '13');
INSERT INTO `sys_roles_permissions` VALUES ('64', '49', '13');
INSERT INTO `sys_roles_permissions` VALUES ('65', '50', '13');
INSERT INTO `sys_roles_permissions` VALUES ('66', '6', '13');
INSERT INTO `sys_roles_permissions` VALUES ('67', '2', '14');
INSERT INTO `sys_roles_permissions` VALUES ('68', '3', '14');
INSERT INTO `sys_roles_permissions` VALUES ('69', '43', '14');
INSERT INTO `sys_roles_permissions` VALUES ('70', '4', '14');
INSERT INTO `sys_roles_permissions` VALUES ('71', '51', '14');
INSERT INTO `sys_roles_permissions` VALUES ('72', '49', '14');
INSERT INTO `sys_roles_permissions` VALUES ('73', '50', '14');
INSERT INTO `sys_roles_permissions` VALUES ('74', '6', '14');
INSERT INTO `sys_roles_permissions` VALUES ('308', '2', '7');
INSERT INTO `sys_roles_permissions` VALUES ('309', '3', '7');
INSERT INTO `sys_roles_permissions` VALUES ('310', '43', '7');
INSERT INTO `sys_roles_permissions` VALUES ('311', '69', '7');
INSERT INTO `sys_roles_permissions` VALUES ('312', '4', '7');
INSERT INTO `sys_roles_permissions` VALUES ('313', '49', '7');
INSERT INTO `sys_roles_permissions` VALUES ('314', '6', '7');
INSERT INTO `sys_roles_permissions` VALUES ('315', '65', '7');
INSERT INTO `sys_roles_permissions` VALUES ('316', '52', '7');
INSERT INTO `sys_roles_permissions` VALUES ('317', '53', '7');
INSERT INTO `sys_roles_permissions` VALUES ('318', '54', '7');
INSERT INTO `sys_roles_permissions` VALUES ('319', '55', '7');
INSERT INTO `sys_roles_permissions` VALUES ('320', '56', '7');
INSERT INTO `sys_roles_permissions` VALUES ('321', '77', '7');
INSERT INTO `sys_roles_permissions` VALUES ('322', '78', '7');
INSERT INTO `sys_roles_permissions` VALUES ('323', '79', '7');
INSERT INTO `sys_roles_permissions` VALUES ('324', '80', '7');
INSERT INTO `sys_roles_permissions` VALUES ('325', '57', '7');
INSERT INTO `sys_roles_permissions` VALUES ('326', '58', '7');
INSERT INTO `sys_roles_permissions` VALUES ('327', '59', '7');
INSERT INTO `sys_roles_permissions` VALUES ('328', '60', '7');
INSERT INTO `sys_roles_permissions` VALUES ('329', '61', '7');
INSERT INTO `sys_roles_permissions` VALUES ('330', '62', '7');
INSERT INTO `sys_roles_permissions` VALUES ('331', '63', '7');
INSERT INTO `sys_roles_permissions` VALUES ('332', '64', '7');
INSERT INTO `sys_roles_permissions` VALUES ('333', '68', '7');
INSERT INTO `sys_roles_permissions` VALUES ('334', '76', '7');
INSERT INTO `sys_roles_permissions` VALUES ('335', '72', '7');
INSERT INTO `sys_roles_permissions` VALUES ('336', '73', '7');
INSERT INTO `sys_roles_permissions` VALUES ('337', '74', '7');
INSERT INTO `sys_roles_permissions` VALUES ('338', '75', '7');
INSERT INTO `sys_roles_permissions` VALUES ('380', '2', '6');
INSERT INTO `sys_roles_permissions` VALUES ('381', '3', '6');
INSERT INTO `sys_roles_permissions` VALUES ('382', '43', '6');
INSERT INTO `sys_roles_permissions` VALUES ('383', '69', '6');
INSERT INTO `sys_roles_permissions` VALUES ('384', '70', '6');
INSERT INTO `sys_roles_permissions` VALUES ('385', '71', '6');
INSERT INTO `sys_roles_permissions` VALUES ('386', '4', '6');
INSERT INTO `sys_roles_permissions` VALUES ('387', '51', '6');
INSERT INTO `sys_roles_permissions` VALUES ('388', '49', '6');
INSERT INTO `sys_roles_permissions` VALUES ('389', '50', '6');
INSERT INTO `sys_roles_permissions` VALUES ('390', '6', '6');
INSERT INTO `sys_roles_permissions` VALUES ('391', '65', '6');
INSERT INTO `sys_roles_permissions` VALUES ('392', '66', '6');
INSERT INTO `sys_roles_permissions` VALUES ('393', '67', '6');
INSERT INTO `sys_roles_permissions` VALUES ('394', '52', '6');
INSERT INTO `sys_roles_permissions` VALUES ('395', '53', '6');
INSERT INTO `sys_roles_permissions` VALUES ('396', '54', '6');
INSERT INTO `sys_roles_permissions` VALUES ('397', '55', '6');
INSERT INTO `sys_roles_permissions` VALUES ('398', '56', '6');
INSERT INTO `sys_roles_permissions` VALUES ('399', '77', '6');
INSERT INTO `sys_roles_permissions` VALUES ('400', '78', '6');
INSERT INTO `sys_roles_permissions` VALUES ('401', '79', '6');
INSERT INTO `sys_roles_permissions` VALUES ('402', '80', '6');
INSERT INTO `sys_roles_permissions` VALUES ('403', '57', '6');
INSERT INTO `sys_roles_permissions` VALUES ('404', '58', '6');
INSERT INTO `sys_roles_permissions` VALUES ('405', '59', '6');
INSERT INTO `sys_roles_permissions` VALUES ('406', '60', '6');
INSERT INTO `sys_roles_permissions` VALUES ('407', '61', '6');
INSERT INTO `sys_roles_permissions` VALUES ('408', '62', '6');
INSERT INTO `sys_roles_permissions` VALUES ('409', '63', '6');
INSERT INTO `sys_roles_permissions` VALUES ('410', '64', '6');
INSERT INTO `sys_roles_permissions` VALUES ('411', '82', '6');
INSERT INTO `sys_roles_permissions` VALUES ('412', '83', '6');
INSERT INTO `sys_roles_permissions` VALUES ('413', '84', '6');
INSERT INTO `sys_roles_permissions` VALUES ('414', '68', '6');
INSERT INTO `sys_roles_permissions` VALUES ('415', '76', '6');
INSERT INTO `sys_roles_permissions` VALUES ('416', '72', '6');
INSERT INTO `sys_roles_permissions` VALUES ('417', '73', '6');
INSERT INTO `sys_roles_permissions` VALUES ('418', '74', '6');
INSERT INTO `sys_roles_permissions` VALUES ('419', '75', '6');
INSERT INTO `sys_roles_permissions` VALUES ('444', '2', '8');
INSERT INTO `sys_roles_permissions` VALUES ('445', '3', '8');
INSERT INTO `sys_roles_permissions` VALUES ('446', '43', '8');
INSERT INTO `sys_roles_permissions` VALUES ('447', '4', '8');
INSERT INTO `sys_roles_permissions` VALUES ('448', '51', '8');
INSERT INTO `sys_roles_permissions` VALUES ('449', '49', '8');
INSERT INTO `sys_roles_permissions` VALUES ('450', '50', '8');
INSERT INTO `sys_roles_permissions` VALUES ('451', '6', '8');
INSERT INTO `sys_roles_permissions` VALUES ('452', '52', '8');
INSERT INTO `sys_roles_permissions` VALUES ('453', '53', '8');
INSERT INTO `sys_roles_permissions` VALUES ('454', '54', '8');
INSERT INTO `sys_roles_permissions` VALUES ('455', '55', '8');
INSERT INTO `sys_roles_permissions` VALUES ('456', '56', '8');
INSERT INTO `sys_roles_permissions` VALUES ('457', '86', '8');
INSERT INTO `sys_roles_permissions` VALUES ('458', '77', '8');
INSERT INTO `sys_roles_permissions` VALUES ('459', '80', '8');
INSERT INTO `sys_roles_permissions` VALUES ('460', '57', '8');
INSERT INTO `sys_roles_permissions` VALUES ('461', '58', '8');
INSERT INTO `sys_roles_permissions` VALUES ('462', '59', '8');
INSERT INTO `sys_roles_permissions` VALUES ('463', '60', '8');
INSERT INTO `sys_roles_permissions` VALUES ('464', '61', '8');
INSERT INTO `sys_roles_permissions` VALUES ('465', '62', '8');
INSERT INTO `sys_roles_permissions` VALUES ('466', '63', '8');
INSERT INTO `sys_roles_permissions` VALUES ('467', '64', '8');
INSERT INTO `sys_roles_permissions` VALUES ('468', '68', '8');
INSERT INTO `sys_roles_permissions` VALUES ('469', '76', '8');
INSERT INTO `sys_roles_permissions` VALUES ('470', '72', '8');
INSERT INTO `sys_roles_permissions` VALUES ('471', '73', '8');
INSERT INTO `sys_roles_permissions` VALUES ('472', '74', '8');
INSERT INTO `sys_roles_permissions` VALUES ('473', '75', '8');

-- ----------------------------
-- Table structure for sys_site_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_site_info`;
CREATE TABLE `sys_site_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_addr` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_fixed_phone` varchar(255) DEFAULT NULL,
  `contact_mobile_phone` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `site_logo` varchar(255) DEFAULT NULL,
  `site_modal` varchar(255) DEFAULT NULL,
  `site_template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_site_info
-- ----------------------------
INSERT INTO `sys_site_info` VALUES ('1', '四川省成都市高新区软件园F区菁蓉国际2', 'thinkcms@163.com', '028-8329888', '15928165825', '船舶制品有限公司', '-专注业船舶制品有限公司制造', 'http://om6onp8sp.bkt.clouddn.com/85f074ca-aa1d-4168-81f2-9c16f026fb52.jpg', null, 'default');

-- ----------------------------
-- Table structure for sys_slide_img
-- ----------------------------
DROP TABLE IF EXISTS `sys_slide_img`;
CREATE TABLE `sys_slide_img` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alt` varchar(255) DEFAULT NULL,
  `available` bit(1) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `show_order` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_slide_img
-- ----------------------------
INSERT INTO `sys_slide_img` VALUES ('1', 'banner图片2', '', 'http://om6onp8sp.bkt.clouddn.com/e25f6141-e09e-48ab-bfec-a70bd48240a5.jpg', '1', '图片摘要2图片摘要2图片摘要2图片摘要2图片摘要2图片摘要2图片摘要2图片摘要2');
INSERT INTO `sys_slide_img` VALUES ('3', 'banner图片1', '', 'http://om6onp8sp.bkt.clouddn.com/13f82457-5fc0-4d71-b99b-cf7f21b16988.jpg', '1', '图片摘要3图片摘要3图片摘要3图片摘要3图片摘要3图片摘要3图片摘要3图片摘要3');
INSERT INTO `sys_slide_img` VALUES ('4', 'banner图片3', '', 'http://om6onp8sp.bkt.clouddn.com/2227313d-44b3-4e42-950f-c1bc17db6919.jpg', '2', '图片摘要图片摘要图片摘要图片摘要图片摘要图片摘要图片摘要图片摘要');

-- ----------------------------
-- Table structure for sys_users
-- ----------------------------
DROP TABLE IF EXISTS `sys_users`;
CREATE TABLE `sys_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) DEFAULT NULL,
  `locked` bit(1) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `nice_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_users
-- ----------------------------
INSERT INTO `sys_users` VALUES ('2', '', '\0', '15928165825', '管理员', '21685a3b449544636cf3bd420425d0a9', '5de34419eceb8ccb6f85ccc8282cdda7', 'admin');
INSERT INTO `sys_users` VALUES ('6', '文化传播部', '\0', '15988888888', '托尔斯泰', 'e97df53dbfde146f38c8b7289fcd5527', '8db950d7ae645a9d465faeb399219a2c', 'wl');
INSERT INTO `sys_users` VALUES ('9', '', '\0', '', '猴子', '9d043ed7c324a9ef9234a7a2067f47a7', 'bc12bcde71c1bb227996e4d2a6f0c3ec', 'houzi');

-- ----------------------------
-- Table structure for sys_users_roles
-- ----------------------------
DROP TABLE IF EXISTS `sys_users_roles`;
CREATE TABLE `sys_users_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_users_roles
-- ----------------------------
INSERT INTO `sys_users_roles` VALUES ('10', '6', '6');
INSERT INTO `sys_users_roles` VALUES ('16', '8', '8');
INSERT INTO `sys_users_roles` VALUES ('17', '8', '9');
INSERT INTO `sys_users_roles` VALUES ('18', '6', '2');

-- ----------------------------
-- Table structure for zq_stadium
-- ----------------------------
DROP TABLE IF EXISTS `zq_stadium`;
CREATE TABLE `zq_stadium` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `addr` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `contMan` varchar(255) DEFAULT NULL,
  `contPhone` varchar(255) DEFAULT NULL,
  `contQQ` varchar(255) DEFAULT NULL,
  `contQqun` varchar(255) DEFAULT NULL,
  `contWb` varchar(255) DEFAULT NULL,
  `contWx` varchar(255) DEFAULT NULL,
  `lamplight` varchar(255) DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `openTime` varchar(255) DEFAULT NULL,
  `showImg` varchar(255) DEFAULT NULL,
  `siteInfo` varchar(255) DEFAULT NULL,
  `siteType` varchar(255) DEFAULT NULL,
  `sod` varchar(255) DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zq_stadium
-- ----------------------------
INSERT INTO `zq_stadium` VALUES ('23', '武侯区昆华路919-10号', '武侯区->大源', '成都', '老杨', '028-86035000', '无', '无', '无', '无', '有灯光', null, '猎豹足球俱乐部', '9:00-22:00', 'http://7xkeg5.com1.z0.glb.clouddn.com/5567f400-33b8-4f29-8005-94772b8d1087.jpg', '一共三块场地,在大源公园里面.有两块是平时是开放的免费球场.', '室外五人制|5-7人', '人造草皮', '2016-08-25 15:12:57');
INSERT INTO `zq_stadium` VALUES ('24', '淡淡的', '高新区', '成都', '无无', '无', '无', '无', '无', '无', '有灯光', null, '绿洲足球俱乐部', '8：00-20：00', 'http://om6onp8sp.bkt.clouddn.com/99750a36-0f0b-45a6-888c-d6ee5c1f393e.jpg', '无', '室外五人制', '人工草皮', '2017-03-03 10:24:07');
