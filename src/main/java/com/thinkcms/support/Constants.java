package com.thinkcms.support;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Maps;

public class Constants {
	/**
	 * 默认每页显示条数
	 */
	public static final int PAGE_SIZE = 10;
	/**
	 * 默认静态页面存放路径
	 */
	public static final String HTML_PATTH = "html";
	/**
	 * 默认模板存放根路径
	 */
	public static final String BASE_TEMPLATE = "template";
	/**
	 * 默认列表页模板存放路径
	 */
	public static final String LIST_TEMPLATE = "/template/list/";
	/**
	 * 默认内容页模板存放路径
	 */
	public static final String CONTENT_TEMPLATE = "/template/content/";
	/**
	 * 默认页面碎片模板存放路径
	 */
	public static final String CONTENT_PUBLIC = "/template/public/";
	
	/**
	 * shiro用户强制退出的属性
	 */
	public static final String SESSION_FORCE_LOGOUT_KEY = "session.force.logout";
	
	/**
	 * DataTable把前台传递过来的json数据转成map
	 * @param json
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static  Map<String,Object> convertToMap(String json){
		List<Object>	 list = JSONArray.parseArray(json);
        Map<String,Object>	map =Maps.newHashMap();
        for(Object ob:list){
            Map<String,Object> param =(Map<String, Object>) ob;
            map.put(param.get("name")+"",param.get("value"));
        }
        return map;
	}
	
}
