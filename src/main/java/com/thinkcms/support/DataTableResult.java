package com.thinkcms.support;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class DataTableResult {
	private Integer sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<?> aaData = Lists.newArrayList();
	private String sColumns;
	public Integer getSEcho() {
		return sEcho;
	}
	public void setSEcho(Integer sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getITotalRecords() {
		return iTotalRecords;
	}
	public void setITotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getITotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setITotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<?> getAaData() {
		return aaData;
	}
	public void setAaData(List<?> aaData) {
		this.aaData = aaData;
	}
	public String getSColumns() {
		return sColumns;
	}
	public void setSColumns(String sColumns) {
		this.sColumns = sColumns;
	}
	
}
