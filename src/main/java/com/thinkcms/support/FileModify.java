package com.thinkcms.support;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class FileModify {
	private String replaceStr;
	
	public String getReplaceStr() {
		return replaceStr;
	}

	public void setReplaceStr(String replaceStr) {
		this.replaceStr = replaceStr;
	}

	public FileModify() {
		// TODO Auto-generated constructor stub
	}
	
	public FileModify(String replaceStr) {
		// TODO Auto-generated constructor stub
		this.replaceStr = replaceStr;
	}
	/**
     * 读取文件内容
     * 
     * @param filePath
     * @return
     */
    public String read(String filePath) {
        BufferedReader br = null;
        String line = null;
        StringBuffer buf = new StringBuffer();
        
        try {
            // 根据文件路径创建缓冲输入流
        	File f = new File(filePath);
            br = new BufferedReader(new InputStreamReader(new FileInputStream(f),"UTF-8"));
            
            // 循环读取文件的每一行, 对需要修改的行进行修改, 放入缓冲对象中
            while ((line = br.readLine()) != null) {
                // 此处根据实际需要修改某些行的内容
            	if(replaceStr != null && !"".equals(replaceStr)){
	                if (line.contains(replaceStr)) {
	                	line = line.replace(replaceStr, "");
	                	buf.append(line);
	                }else{
	                	buf.append(line);
	                }
            	}
                buf.append(System.getProperty("line.separator"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭流
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    br = null;
                }
            }
        }
        
        return buf.toString();
    }
    
    /**
     * 将内容回写到文件中
     * 
     * @param filePath
     * @param content
     */
    public void write(String filePath, String content) {
    	PrintWriter  bw = null;
        
        try {
        	File f = new File(filePath);
            // 根据文件路径创建缓冲输出流
            bw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f),"UTF-8"));
            // 将内容写入文件中
            bw.write(content);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭流
            if (bw != null) {
                bw.close();
            }
        }
    }
}
