package com.thinkcms.support;

import java.util.List;

/**
 * Ztree 树
 * @author Administrator
 *
 */
public class ZtreeVO {
	private String name;
	
	private String url;
	
	private Boolean open;
	
	private Boolean isParent;
	
	private Long categoryId;
	
	private List<ZtreeVO> children;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}
	
	public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}

	public List<ZtreeVO> getChildren() {
		return children;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public void setChildren(List<ZtreeVO> children) {
		this.children = children;
	}

}
