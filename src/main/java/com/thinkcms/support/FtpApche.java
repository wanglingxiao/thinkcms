package com.thinkcms.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FtpApche {
	private static Logger LOGGER = LoggerFactory.getLogger(FtpApche.class);
    private static String url = "zhuceyonghum.gotoip11.com";
    private static String username = "zhuceyonghum";
    private static String password	="22oo8822";
    private static int port = 21;
    private static String path = "/wwwroot";
    private static final DecimalFormat DF = new DecimalFormat("#.##");
    
    private static FTPClient connect(){
    	FTPClient ftpClient = new FTPClient();
    	int reply;
    	try {
    		if(port == 21){
    			ftpClient.connect(url);
    		}else{
    			ftpClient.connect(url, port);
    		}
			ftpClient.login(username, password);
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			// 检验是否连接成功
            reply = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
                System.out.println("连接失败");
                ftpClient.disconnect();
                return null;
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return ftpClient;
    }
    
    /**
     * 切换工作目录，远程目录不存在时，创建目录
     * @param client
     * @throws IOException
     */
    private static FTPClient changeWorkingDirectory(FTPClient client,String path){
        if(path!=null&&!"".equals(path)){
        	path = path.replace("\\", "/");
            boolean ok;
			try {
				ok = client.changeWorkingDirectory(path);
				if(!ok){
	                //ftpPath 不存在，手动创建目录
	                StringTokenizer token = new StringTokenizer(path,"\\//");
	                while(token.hasMoreTokens()){
	                    String p = token.nextToken();
	                    client.makeDirectory(p);
	                    client.changeWorkingDirectory(p);
	                }
	            }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return client;
    }
    /**
     * Description: 向FTP服务器上传文件
     * 
     * @Version1.0
     * @param url
     *            FTP服务器hostname
     * @param port
     *            FTP服务器端口
     * @param username
     *            FTP登录账号
     * @param password
     *            FTP登录密码
     * @param path
     *            FTP服务器保存目录,如果是根目录则为“/”
     * @param filename
     *            上传到FTP服务器上的文件名
     * @param input
     *            本地文件输入流
     * @return 成功返回true，否则返回false
     * @throws IOException 
     */
    public  boolean uploadFiles( String baseUrl,String folder,String replaystr,HttpServletRequest request) {
        	boolean result = false;
    	FTPClient  ftpClient =  connect();
    	if(ftpClient == null){
    		LOGGER.info("连接FTP服务器["+url+":"+port+"]失败！");
    		return result;
    	}
    	String urlpath = folder.replace(baseUrl, "");
    	LOGGER.info("<-----urlpath的值为"+urlpath);
    	ftpClient = changeWorkingDirectory(ftpClient,path+urlpath);
    	File file = new File(folder);
    	File [] files = file.listFiles();
    	FileModify obj = new FileModify(replaystr);
    	for (File a : files) {
    		if(a.isDirectory())
            {
    			uploadFiles(baseUrl,a.getAbsolutePath(),replaystr,request);
            }else{
            	if(a.getName().endsWith(".html")){
            		obj.write(a.getAbsolutePath(), obj.read(a.getAbsolutePath())); // 读取修改文件
            	}
            	if(!a.getName().contains("_sp")){
            		uploadfile(ftpClient,a,request);
            	}
            }
		}
    	result=true;
    	try {
			ftpClient.disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return result;
    }
    
    public  boolean uploadOneCategoryFiles( String urlpath,String folder,String replaystr,HttpServletRequest request) {
    	boolean result = false;
		FTPClient  ftpClient =  connect();
		if(ftpClient == null){
			LOGGER.info("连接FTP服务器["+url+":"+port+"]失败！");
			return result;
		}
		ftpClient = changeWorkingDirectory(ftpClient,path+urlpath);
		File file = new File(folder);
		File [] files = file.listFiles();
		FileModify obj = new FileModify(replaystr);
		for (File a : files) {
			if(a.isFile())
	        {
				if(a.getName().endsWith(".html")){
	        		obj.write(a.getAbsolutePath(), obj.read(a.getAbsolutePath())); // 读取修改文件
	        	}
	        	if(!a.getName().contains("_sp")){
            		uploadfile(ftpClient,a,request);
            	}
	        	
	        }
		}
		result=true;
	try {
		ftpClient.disconnect();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return result;
}
    
    public static void uploadfile(FTPClient  ftpClient,File file,HttpServletRequest request){
    	FileInputStream fis = null;
    	try {
			fis = new FileInputStream(file);
			LOGGER.info(">>>开始上传文件："+file.getName());
            boolean ok = ftpClient.storeFile(file.getName(), fis);
            if (ok) {
            	request.getSession().setAttribute("message", (String.format(">>>上传文件'"+file.getName()+"'成功：大小：%s", formatSize(file.length())))) ;
            }else{
            	request.getSession().setAttribute("message", (String.format(">>>上传文件'"+file.getName()+"'失败：大小：%s", formatSize(file.length()))));
            }
		} catch (Exception e) {
			// TODO Auto-generated catch block
            e.printStackTrace();
		}finally{
			 try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    
    /**
     * 格式化文件大小（B，KB，MB，GB）
     * @param size
     * @return
     */
    private static String formatSize(long size){
        if(size<1024){
            return size + " B";
        }else if(size<1024*1024){
            return size/1024 + " KB";
        }else if(size<1024*1024*1024){
            return (size/(1024*1024)) + " MB";
        }else{
            double gb = size/(1024*1024*1024);
            return DF.format(gb)+" GB";
        }
    }



    
    public static void main(String[] args) {
//    	String folder = "E:\\companyWebsite\\dist";
//    	uploadFiles(folder,folder);
    	String file = "E:\\companyWebsite\\dist\\index.html";
    	FileModify obj = new FileModify();
    	obj.write(file, obj.read(file)); // 读取修改文件
    	System.out.println("文件修改完成");
    }
    
    
    public static void test(String basefolder,String folder,String nextname){
    }
    
}