package com.thinkcms.support;

/**
 * 一个节点的初始状态。
 * @author Administrator
 *
 */
public class State {
	/**
	 * 指示一个节点是否处于checked状态，用一个checkbox图标表示。
	 */
	private Boolean checked;
	/**
	 * 指示一个节点是否处于disabled状态。（不是selectable，expandable或checkable）
	 */
	private Boolean disabled;
	/**
	 * 指示一个节点是否处于展开状态。
	 */
	private Boolean expanded;
	/**
	 * 指示一个节点是否可以被选择。
	 */
	private Boolean selected;
	public Boolean getChecked() {
		return checked;
	}
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	public Boolean getDisabled() {
		return disabled;
	}
	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
	public Boolean getExpanded() {
		return expanded;
	}
	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	
}
