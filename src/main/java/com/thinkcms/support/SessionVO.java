package com.thinkcms.support;

import java.io.Serializable;

public class SessionVO {
	private String id;
	private String username;
	private String startTimestamp;
	private String lastAccessTime;
	private long timeout;
	private String host;
	private Boolean isLoginOut;
	public Serializable getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getStartTimestamp() {
		return startTimestamp;
	}
	public void setStartTimestamp(String startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
	public String getLastAccessTime() {
		return lastAccessTime;
	}
	public void setLastAccessTime(String lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}
	public long getTimeout() {
		return timeout;
	}
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Boolean getIsLoginOut() {
		return isLoginOut;
	}
	public void setIsLoginOut(Boolean isLoginOut) {
		this.isLoginOut = isLoginOut;
	}
	
}
