package com.thinkcms.support;

import java.util.List;

/**
 * bootstrap  tree树
 * @author Administrator
 *
 */
public class TreeVO {
	/**
	 * 列表树节点上的文本
	 */
	private String text;
	/**
	 * 列表树节点上的图标
	 */
	private String icon;
	/**
	 * 当某个节点被选择后显示的图标，通常是节点左边的图标。
	 */
	private String selectedIcon;
	/**
	 * 节点的前景色，覆盖全局的前景色选项。
	 */
	private String color;
	/**
	 * 节点的背景色，覆盖全局的背景色选项。
	 */
	private String backColor;
	/**
	 * 结合全局enableLinks选项为列表树节点指定URL。
	 */
	private String href;
	/**
	 * 指定列表树的节点是否可选择。设置为false将使节点展开，并且不能被选择。
	 */
	private Boolean selectable;
	/**
	 * 一个节点的初始状态。
	 */
	private State state;
	/**
	 * 通过结合全局showTags选项来在列表树节点的右边添加额外的信息。
	 */
	private String	tags;
	
	/**
	 *树的子节点
	 */
	private List<TreeVO> nodes;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getSelectedIcon() {
		return selectedIcon;
	}

	public void setSelectedIcon(String selectedIcon) {
		this.selectedIcon = selectedIcon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBackColor() {
		return backColor;
	}

	public void setBackColor(String backColor) {
		this.backColor = backColor;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Boolean getSelectable() {
		return selectable;
	}

	public void setSelectable(Boolean selectable) {
		this.selectable = selectable;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public List<TreeVO> getNodes() {
		return nodes;
	}

	public void setNodes(List<TreeVO> nodes) {
		this.nodes = nodes;
	}
	
}
