package com.thinkcms.realm;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.session.Session;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

import com.thinkcms.support.Constants;

public class ForceLogoutFilter extends AccessControlFilter {

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)
			throws Exception {
		// TODO Auto-generated method stub
		Session session = getSubject(request, response).getSession(false);  
        if(session == null) {  
            return true;  
        }  
        return session.getAttribute(Constants.SESSION_FORCE_LOGOUT_KEY) == null;	
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		try {  
            getSubject(request, response).logout();//强制退出  
        } catch (Exception e) {/*ignore exception*/}  
    	WebUtils.issueRedirect(request, response, getLoginUrl());  
    	return false; 
	}
}
