package com.thinkcms.template;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.thinkcms.entity.Article;
import com.thinkcms.entity.Category;
import com.thinkcms.service.ArticleService;
import com.thinkcms.service.CategoryService;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class ArticleDirective extends BaseDirective implements TemplateDirectiveModel{
	private ArticleService articleService; 
	private CategoryService categoryService;
 
	public ArticleDirective() {
		// TODO Auto-generated constructor stub
	}
	
	public ArticleDirective(ArticleService articleService,CategoryService categoryService) {
		// TODO Auto-generated constructor stub
		this.articleService = articleService;
		this.categoryService = categoryService;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		// TODO Auto-generated method stub
		long cid = 0;
        int limit = 10;
        int start = 1;
        Iterator iterator=params.entrySet().iterator();  
        while(iterator.hasNext()) {
        	Map.Entry<String,TemplateModel> param=(Map.Entry<String, TemplateModel>)iterator.next();  
            String paramName=param.getKey();  
            TemplateModel paramValue=param.getValue();
            if(paramName.equals("cid"))  
            {  
            	cid=getLong(paramName,paramValue);  
                if(cid<=0)  
                {  
                    throw new TemplateModelException("栏目ID错误");  
                }  
            } 
            if(paramName.equals("limit"))  
            {  
            	limit=getInt(paramName,paramValue);  
            	if(limit<=0)  
            	{  
            		throw new TemplateModelException("获取数量错误");  
            	}  
            } 
            if(paramName.equals("start"))  
            {  
            	start=getInt(paramName,paramValue);  
            	if(start<=0)  
            	{  
            		throw new TemplateModelException("开始条数错误");  
            	}  
            } 
        }
        //栏目里的文章数据
        List<Article> articlea = getArticles(cid, limit, start);
        
        Category c = categoryService.getCategory(cid);
        
        Map<String,Object> map = Maps.newHashMap();
        map.put("articles", articlea);
        map.put("category", c);
        if (body != null) {
        	loopVars[0] = WrappingTemplateModel.getDefaultObjectWrapper().wrap(map);
        	// 执行标签内容(same as <#nested> in FTL).   
            body.render(env.getOut());
        }else { 
            throw new RuntimeException("missing body"); 
        }
	}
	
	private List<Article> getArticles(Long cid,int limit,int start){
		Set<Long> ids = Sets.newHashSet();
		ids = getAllChildrenId(cid,ids);
		DetachedCriteria dc = DetachedCriteria.forClass(Article.class);
		dc.add(Restrictions.in("categoryId", ids));
		dc.add(Restrictions.eq("available", true));
		dc.addOrder(Order.desc("modifyDate"));
		dc.addOrder(Order.desc("createDate"));
        List<Article> articleList = articleService.findByParams(dc,start,limit);
        return articleList;
	}
	
	private Set<Long> getAllChildrenId(Long cid,Set<Long> ids){
		DetachedCriteria cd = DetachedCriteria.forClass(Category.class);
		cd.add(Restrictions.eq("parentId", cid));
		cd.add(Restrictions.eq("available", true));
		List<Category> childlist = categoryService.findList(cd);
		if(childlist != null && childlist.size()>0){
			for (Category c : childlist) {
				ids.add(c.getId());
				getAllChildrenId(c.getId(),ids);
			}
		}
		ids.add(cid);
		return ids;
	}

}
