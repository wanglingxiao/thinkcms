package com.thinkcms.template;

import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

public class BaseDirective {
	public long getLong(String paramName, TemplateModel paramValue) throws TemplateModelException{
		// TODO Auto-generated method stub
		 if(paramValue==null)  
	        {  
	            return 0;  
	        }  
	        else   
	        {  
	            if(paramValue instanceof TemplateScalarModel)  //是字符串  
	            {  
	                String value=((TemplateScalarModel)paramValue).getAsString();  
	                return Long.valueOf(value);  
	            }  
	            else if(paramValue instanceof TemplateNumberModel)  //数字   
	            {  
	                return ((TemplateNumberModel)paramValue).getAsNumber().longValue();  
	            }  
	            else   
	            {  
	                throw new TemplateModelException("Long转换异常!");  
	            }  
	        }  
	}
	
	public	int getInt(String paramName,TemplateModel paramValue) throws TemplateModelException  
    {  
        if(paramValue==null)  
        {  
            return 0;  
        }  
        else   
        {  
            if(paramValue instanceof TemplateScalarModel)  //是字符串  
            {  
                String value=((TemplateScalarModel)paramValue).getAsString();  
                return Integer.valueOf(value);  
            }  
            else if(paramValue instanceof TemplateNumberModel)  //数字   
            {  
                return ((TemplateNumberModel)paramValue).getAsNumber().intValue();  
            }  
            else   
            {  
                throw new TemplateModelException("int转换异常!");  
            }  
        }  
    }  
	
	public  boolean getBoolean(String paramName,TemplateModel paramValue) throws TemplateModelException  
    {  
        if(paramValue==null)  
        {  
            return false;  
        }  
        else   
        {  
            if(paramValue instanceof TemplateScalarModel)  //是字符串  
            {  
                String value=((TemplateScalarModel)paramValue).getAsString();  
                return Boolean.valueOf(value);  
            }  
            else if(paramValue instanceof TemplateBooleanModel)  //boolean   
            {  
                return ((TemplateBooleanModel)paramValue).getAsBoolean();  
            }  
            else   
            {  
                throw new TemplateModelException("boolean转换异常!");  
            }  
        }  
    }  
}
