package com.thinkcms.template;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.thinkcms.entity.SlideImg;
import com.thinkcms.service.SlideImgService;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class SlideImgDirective extends BaseDirective implements TemplateDirectiveModel {
	private SlideImgService slideImgService;

	public SlideImgDirective() {
		// TODO Auto-generated constructor stub
	}

	public SlideImgDirective(SlideImgService slideImgService) {
		// TODO Auto-generated constructor stub
		this.slideImgService = slideImgService;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		// TODO Auto-generated method stub
		int limit = 0;
		Iterator iterator = params.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, TemplateModel> param = (Map.Entry<String, TemplateModel>) iterator.next();
			String paramName = param.getKey();
			TemplateModel paramValue = param.getValue();
			if (paramName.equals("limit")) {
				limit = getInt(paramName, paramValue);
				if (limit<=0) {
					throw new TemplateModelException("模板数据未开启");
				}
			}
		}
		List<SlideImg> data = getData(limit);
		if (body != null) {
        	loopVars[0] = WrappingTemplateModel.getDefaultObjectWrapper().wrap(data);
        	// 执行标签内容(same as <#nested> in FTL).   
            body.render(env.getOut());
        }else { 
            throw new RuntimeException("missing body"); 
        }
	}
	
	private List<SlideImg> getData(int limit){
		DetachedCriteria dimg = DetachedCriteria.forClass(SlideImg.class);
		dimg.add(Restrictions.eq("available", true));
		dimg.addOrder(Order.desc("showOrder"));
		List<SlideImg> imglist = slideImgService.findListByParamsAndLimit(dimg, limit);
		return imglist;
	}

}
