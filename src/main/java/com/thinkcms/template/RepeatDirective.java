package com.thinkcms.template;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

public class RepeatDirective extends BaseDirective implements TemplateDirectiveModel {

	private static final String COUNT="count";  
    private static final String HR="hr";
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		// TODO Auto-generated method stub
		 if(body==null)  
	        {  
	            throw new TemplateModelException("null body");  
	        }  
	        else   
	        {  
	            int count=0;  
	            boolean hr=false;  
	            Iterator iterator=params.entrySet().iterator();  
	            while(iterator.hasNext())  
	            {  
	                Map.Entry<String,TemplateModel> param=(Map.Entry<String, TemplateModel>)iterator.next();  
	                String paramName=param.getKey();  
	                TemplateModel paramValue=param.getValue();  
	                if(paramName.equals(COUNT))  
	                {  
	                    count=getInt(paramName,paramValue);  
	                    if(count<0)  
	                    {  
	                        throw new TemplateModelException("次数不能小于0");  
	                    }  
	                }  
	                if(paramName.equals(HR))  
	                {  
	                    hr=getBoolean(paramName,paramValue);  
	                }  
	            }  
	              
	            Writer out=env.getOut();  
	            for(int i=0;i<count;i++)  
	            {  
	                if(hr && i!=0)  
	                {  
	                    out.write("<hr>");  
	                }  
	                //设置了循环变量  
	                if(loopVars.length>0)  
	                {  
	                    loopVars[0]=new SimpleNumber(i+1);  
	                }  
	                body.render(out);                         
	            }  
	        }  
	}

}
