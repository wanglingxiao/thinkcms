package com.thinkcms.template;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.thinkcms.entity.FriendshipLink;
import com.thinkcms.service.FriendshipLinkService;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

public class FriendLinkDirective extends BaseDirective implements TemplateDirectiveModel {

	private FriendshipLinkService friendshipLinkService;

	public FriendLinkDirective() {
		// TODO Auto-generated constructor stub
	}

	public FriendLinkDirective(FriendshipLinkService friendshipLinkService) {
		// TODO Auto-generated constructor stub
		this.friendshipLinkService = friendshipLinkService;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		// TODO Auto-generated method stub
		int limit = 0;
		Iterator iterator = params.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, TemplateModel> param = (Map.Entry<String, TemplateModel>) iterator.next();
			String paramName = param.getKey();
			TemplateModel paramValue = param.getValue();
			if(paramName.equals("limit"))  
            {  
            	limit=getInt(paramName,paramValue);  
            	if(limit<=0)  
            	{  
            		throw new TemplateModelException("获取数量错误");  
            	}  
            } 
			
		}
		
		List<FriendshipLink> list = getLinkList(limit);
		if (body != null) {
        	loopVars[0] = WrappingTemplateModel.getDefaultObjectWrapper().wrap(list);
        	// 执行标签内容(same as <#nested> in FTL).   
            body.render(env.getOut());
        }else { 
            throw new RuntimeException("missing body"); 
        }
	}

	private List<FriendshipLink> getLinkList(int limit) {
		DetachedCriteria dc = DetachedCriteria.forClass(FriendshipLink.class);
		dc.add(Restrictions.eq("available", true));
		dc.addOrder(Order.desc("showOrder"));
		List<FriendshipLink> list = friendshipLinkService.findListByParams(dc,limit);
		return list;
	}

}
