package com.thinkcms.template;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.thinkcms.entity.Category;
import com.thinkcms.service.CategoryService;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;

public class RootChannelDirective extends BaseDirective implements TemplateDirectiveModel{
	private CategoryService categoryService;
	
	public RootChannelDirective() {
		// TODO Auto-generated constructor stub
	}
	
	public RootChannelDirective(CategoryService categoryService) {
		// TODO Auto-generated constructor stub
		this.categoryService = categoryService;
	}
	
	@SuppressWarnings({ "rawtypes"})
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		// TODO Auto-generated method stub
		List<Category> list = getData();
		if (body != null) {
        	loopVars[0] = WrappingTemplateModel.getDefaultObjectWrapper().wrap(list);
        	// 执行标签内容(same as <#nested> in FTL).   
            body.render(env.getOut());
        }else { 
            throw new RuntimeException("missing body"); 
        }
	}
	
	private List<Category> getData(){
		DetachedCriteria dlanmu = DetachedCriteria.forClass(Category.class);
		dlanmu.add(Restrictions.eq("parentId", (long)0));
		dlanmu.add(Restrictions.eq("available", true));
		dlanmu.addOrder(Order.desc("showOrder"));
		List<Category> lanmu = categoryService.findList(dlanmu);
		for (Category category : lanmu) {
			DetachedCriteria par = DetachedCriteria.forClass(Category.class);
			par.add(Restrictions.eq("available", true));
			par.addOrder(Order.desc("showOrder"));
			par.add(Restrictions.eq("parentId", category.getId()));
			List<Category> child = categoryService.findList(par);
			if(child.size()>0) category.setChildList(child);
		}
		return lanmu;
	}

}
