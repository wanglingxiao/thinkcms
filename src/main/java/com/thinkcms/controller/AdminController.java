package com.thinkcms.controller;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.web.util.SavedRequest;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.thinkcms.entity.Article;
import com.thinkcms.entity.Attachment;
import com.thinkcms.entity.Category;
import com.thinkcms.entity.Permission;
import com.thinkcms.entity.SlideImg;
import com.thinkcms.service.ArticleService;
import com.thinkcms.service.AttachmentService;
import com.thinkcms.service.CategoryService;
import com.thinkcms.service.PermissionService;
import com.thinkcms.service.SlideImgService;
import com.thinkcms.support.Constants;
import com.thinkcms.support.DateUtils;
import com.thinkcms.support.Result;
import com.thinkcms.support.SessionVO;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private PermissionService permissionService;
	@Autowired
	private ArticleService articleService;
	@Autowired
	private AttachmentService	attachmentService;
	@Autowired
	private CategoryService	categoryService;
	@Autowired
	private	SlideImgService	slideImgService;
	@Autowired
	private SessionDAO	sessionDao;
	
	@RequestMapping("")
	public String admin(Model model) {
		Permission menu = permissionService.getMenu();
		model.addAttribute("menu", menu);
		return "admin/admin";
	}

	@RequiresAuthentication
	@RequestMapping("/index")
	public String index(Model model) {
		//统计文章信息
		DetachedCriteria articleC = DetachedCriteria.forClass(Article.class);
		articleC.add(Restrictions.eq("available", true));
		List<Article>	articlelist = articleService.findByParams(articleC);
		model.addAttribute("articleCount", articlelist.size());
		//统计栏目信息
		DetachedCriteria categoryC = DetachedCriteria.forClass(Category.class);
		categoryC.add(Restrictions.eq("available", true));
		List<Category>	categorylist = categoryService.findList(categoryC);
		model.addAttribute("categoryCount", categorylist.size());
		//统计banner图片
		DetachedCriteria slideImgC = DetachedCriteria.forClass(SlideImg.class);
		slideImgC.add(Restrictions.eq("available", true));
		List<SlideImg>	 slideImglist = slideImgService.findListByParams(slideImgC);
		model.addAttribute("slideImgCount", slideImglist.size());
		//统计附件
		DetachedCriteria attachmentC = DetachedCriteria.forClass(Attachment.class);
		attachmentC.add(Restrictions.eq("available", true));
		List<Attachment>	  attachmentlist = attachmentService.findAll();
		model.addAttribute("attachmentCount", attachmentlist.size());
		
		return "admin/index";
	}
	
	@RequiresRoles("admin")
	@RequestMapping("sessions")
	@ResponseBody
	public Result  sessions(){
		//在线会话
		Collection<Session> sessions =  sessionDao.getActiveSessions();  
		List<SessionVO> list = Lists.newArrayList();
		for (Session session : sessions) {
			SimpleSession ss = (SimpleSession) session;
			SessionVO s = new SessionVO();
			Map<Object,Object> map = ss.getAttributes();
			if(map != null){
				SavedRequest  save = (SavedRequest) map.get("shiroSavedRequest");
				if(save == null){
					s.setUsername((map.get(DefaultSubjectContext.PRINCIPALS_SESSION_KEY)).toString());
					s.setHost(ss.getHost());
					s.setId((String) ss.getId());
					s.setLastAccessTime(DateUtils.format(ss.getLastAccessTime(),"yyyy-MM-dd hh:mm:ss"));
					s.setStartTimestamp(DateUtils.format(ss.getStartTimestamp(),"yyyy-MM-dd hh:mm:ss"));
					s.setTimeout(ss.getTimeout());
					s.setIsLoginOut(false);
					list.add(s);
				}
			}
		}
		Result result = new Result();
		result.setObj(list);
		result.setSuccess(true);
		return result;
	}
	
	/**
	 * 用户强制退出
	 * @param sessionId
	 * @return
	 */
	@RequiresPermissions("admin")
	@RequestMapping("forceLogout")
	@ResponseBody
	public Result  forceLogout(@RequestParam(value = "sessionId",required=false) String sessionId){
		Result result = new Result();
		if(sessionId == null || "".equals(sessionId)){
			result.setSuccess(false);
			return result;
		}
		try {
			Session session = sessionDao.readSession(sessionId);
			if(session != null) {  
                session.setAttribute(Constants.SESSION_FORCE_LOGOUT_KEY, Boolean.TRUE);  
            }
			result.setSuccess(true);
		} catch (Exception e) {
			// TODO: handle exception
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping("/user")
	public String user() {
		return "admin/user";
	}
}