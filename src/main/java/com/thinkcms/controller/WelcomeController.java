package com.thinkcms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WelcomeController {

	@RequestMapping("/login")
	public String login(HttpServletRequest request, HttpServletResponse response,Model model) {
		// HttpSession session = request.getSession();
		// session.invalidate();
		// response.addHeader("x-frame-options", "DENY");
		String exceptionClassName = (String)request.getAttribute("shiroLoginFailure");
        String error = null;
        if(UnknownAccountException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if(IncorrectCredentialsException.class.getName().equals(exceptionClassName)) {
            error = "用户名/密码错误";
        } else if(exceptionClassName != null) {
            error = "其他错误：" + exceptionClassName;
        }
        model.addAttribute("error", error);
        if(request.getParameter("forceLogout") != null) {
            model.addAttribute("error", "您已经被管理员强制退出，请重新登录");
        }
		return "common/login";
	}

	@RequestMapping("/deny")
	public String dely() {
		return "deny";
	}

	@RequestMapping("/timeOut")
	public String timeOut() {
		return "timeout";
	}

	@RequestMapping("/404")
	public String e404() {
		return "common/404";
	}
}