package com.thinkcms.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkcms.entity.Article;
import com.thinkcms.entity.Category;
import com.thinkcms.service.ArticleService;
import com.thinkcms.service.CategoryService;
import com.thinkcms.support.Constants;
import com.thinkcms.support.DataTableResult;
import com.thinkcms.support.PageDto;
import com.thinkcms.support.Result;
import com.thinkcms.support.State;
import com.thinkcms.support.TreeVO;
import com.thinkcms.support.ZtreeVO;

@Controller
@RequestMapping("/article")
public class ArticleController {
	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
	
	@RequiresPermissions({ "article/list" })
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String index(@RequestParam(value = "categoryId",required=false) Long categoryId,Model model) {
		DetachedCriteria c = DetachedCriteria.forClass(Category.class);
		c.add(Restrictions.eq("parentId", (long)0));
		c.add(Restrictions.eq("available", true));
		List<Category> list = categoryService.findList(c);
		List<ZtreeVO> treeData = Lists.newArrayList();
		for (Category category : list) {
			ZtreeVO tree = setZtree(category);
			treeData.add(tree);
		}
		String str = JSON.toJSONString(treeData);
		System.out.println(str);
		model.addAttribute("treeData", str);
		return "article/index";
	}
	
	private ZtreeVO setZtree(Category category){
		DetachedCriteria c = DetachedCriteria.forClass(Category.class);
		c.add(Restrictions.eq("parentId", category.getId()));
		c.add(Restrictions.eq("available", true));
		List<Category> childs = categoryService.findList(c);
		ZtreeVO ztree = new ZtreeVO();
		ztree.setName(category.getName());
		ztree.setCategoryId(category.getId());
		List<ZtreeVO> childZtree = Lists.newArrayList();
		for (Category child : childs) {
			ZtreeVO tr = setZtree(child);
			childZtree.add(tr);
		}
		if(childZtree.size()>0){
			ztree.setOpen(true);
			ztree.setChildren(childZtree);
		}
		return ztree;
	}
	
	private	TreeVO setTree(Category category,Long categoryId){
		DetachedCriteria c = DetachedCriteria.forClass(Category.class);
		c.add(Restrictions.eq("parentId", category.getId()));
		c.add(Restrictions.eq("available", true));
		List<Category> childs = categoryService.findList(c);
		
		TreeVO tree = new TreeVO();
		tree.setText(category.getName());
		tree.setTags(category.getId().toString());
		State state = new State();
		List<TreeVO> nodes = Lists.newArrayList();
		for (Category child : childs) {
			TreeVO tr = setTree(child,categoryId);
			nodes.add(tr);
		}
		if(nodes!= null && nodes.size()>0){
			state.setExpanded(true);
			tree.setSelectable(false);
			tree.setNodes(nodes);
		}else{
			state.setExpanded(false);
		}
		if(category.getId() == categoryId){
			state.setSelected(true);
		}
		tree.setState(state);
		return tree;
	}

//	@RequiresPermissions({ "article/list" })
//	@RequestMapping(value = "/list", method = RequestMethod.POST)
//	public String list(Long categoryId, String title, String tag,
//			String remark, ReqDto req, Model model) {
//		try {
//			model.addAttribute(
//					"list",
//					articleService.findList(categoryId, title, tag, remark,
//							req.getPageNo(), req.getPageSize()));
//			if(categoryId != null && categoryId != 0){
//				model.addAttribute("categoryId", categoryId);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "common/error";
//		}
//		return "article/list";
//	}
	
	@RequiresPermissions({ "article/list" })
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public DataTableResult listArtical(@RequestParam(value = "categoryId",required=false)Long categoryId,
			String aodata,ServletRequest request){
		DetachedCriteria c = DetachedCriteria.forClass(Article.class);
		c.add(Restrictions.eq("available", true));
		if(categoryId != null){
			c.add(Restrictions.eq("categoryId", categoryId));
		}
//		Result result = new Result();
		Map<String,Object>	map =Constants.convertToMap(aodata);
//		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		String search = (String)map.get("sSearch");
		if(search != null && !"".equals(search)){
			c.add(Restrictions.or(Restrictions.like("title", search,MatchMode.ANYWHERE), Restrictions.like("categoryName", search,MatchMode.ANYWHERE)));
		}
		int echo = (Integer) map.get("sEcho");
		int start = (Integer)map.get("iDisplayStart");
		int pageSize = (Integer)map.get("iDisplayLength");
		int pageNum = start/pageSize==0?1:(start/pageSize+1);
		DataTableResult result = new DataTableResult();
		List<Article> list = Lists.newArrayList();
		try {
			if(pageSize>0){
				PageDto<Article> page = articleService.findList(c, pageNum, pageSize);
				for (Article a : page.getItems()) {
					a.setContent(null);
				}
				list = page.getItems();
				result.setITotalDisplayRecords(page.getTotalCount().intValue());
				result.setITotalRecords(page.getTotalCount().intValue());
			}else{
				list = articleService.findByParams(c);
				result.setITotalDisplayRecords(list.size());
				result.setITotalRecords(list.size());
			}
			result.setSEcho(echo);
			result.setAaData(list);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}
	
	@RequiresPermissions({ "article/add" })
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String toAdd(@RequestParam(value = "categoryId",required=false) Long categoryId,Model model) {
		List<TreeVO>	list = Lists.newArrayList();
		try {
			if(categoryId != null && categoryId != 0){
				Category category = categoryService.getCategory(categoryId);
				TreeVO tree = setTree(category,categoryId);
				list.add(tree);
				//bootstrap tree要想显示在前台，必须把结果放到List对象里
				model.addAttribute("category", JSONObject.toJSONString(list));
			}else{
				Category category = categoryService.getRoot();
				TreeVO tree = setTree(category,category.getId());
				list.add(tree);
				model.addAttribute("category", JSONObject.toJSONString(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "common/error";
		}
		return "article/add";
	}

	@RequiresPermissions({ "article/add" })
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Object add(String data, Model model) {
		if(data == null || "".equals(data)){
			return new Result(false, data);
		}
		Article article = JSON.parseObject(data,Article.class);
		try {
			article.setCreateDate(new Date());
			article = articleService.saveOrUpdate(article);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		return new Result(true, article);
	}

	@RequiresPermissions({ "article/edit" })
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String toEdit(@RequestParam(value = "categoryId",required=false) Long categoryId,Long id, Model model) {
		List<TreeVO>	list = Lists.newArrayList();
		try {
			Article article = articleService.getArticle(id);
			model.addAttribute("entity", article);
			if(categoryId != null && categoryId != 0){
				Category category = categoryService.getCategory(categoryId);
				TreeVO tree = setTree(category,categoryId);
				list.add(tree);
				//bootstrap tree要想显示在前台，必须把结果放到List对象里
				model.addAttribute("category", JSONObject.toJSONString(list));
			}else{
				Category category = categoryService.getRoot();
				TreeVO tree = setTree(category,category.getId());
				list.add(tree);
				model.addAttribute("category", JSONObject.toJSONString(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "common/error";
		}
		return "article/edit";
	}

	@RequiresPermissions({ "article/edit" })
	@ResponseBody
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public Object edit(String data, Model model) {
		if(data == null || "".equals(data)){
			return new Result(false, data);
		}
		Article article = JSON.parseObject(data,Article.class);
		try {
			Article old = articleService.getArticle(article.getId());
			article.setCreateDate(old.getCreateDate());
			article.setModifyDate(new Date());
			article = articleService.saveOrUpdate(article);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		return new Result(true, article);
	}

	@RequiresPermissions({ "article/del" })
	@ResponseBody
	@RequestMapping(value = "/del", method = RequestMethod.POST)
	public Object del(String ids, Model model) {
		try {
			articleService.delBatch(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		return new Result();
	}

	private String getCategoryStr(String selected) {
		Category category = categoryService.getRoot();
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[ {");
		stringBuffer.append("\"text\": \"" + category.getName() + "\",");
		stringBuffer.append("\"tags\": \"" + category.getId() + "\"");
		stringBuffer.append(",\"state\": {");
		stringBuffer.append("\"expanded\": false");
		if (selected.equals(category.getName())) {
			stringBuffer.append(",\"selected\": true");
		}
		stringBuffer.append("}");
		stringBuffer.append(",\"nodes\" : [ ");
		List<Category> level1 = category.getChildList();
		if (level1 != null) {
			for (int i = 0; i < level1.size(); i++) {
				stringBuffer.append(" { ");
				stringBuffer.append("\"tags\": \"" + level1.get(i).getId()
						+ "\",");
				stringBuffer.append(" \"text\" : \"" + level1.get(i).getName()
						+ "\" ");

				stringBuffer.append(",\"state\": {");
				stringBuffer.append("\"expanded\": true");
				if (selected.equals(level1.get(i).getName())) {
					stringBuffer.append(",\"selected\": true");
				}
				stringBuffer.append("}");

				List<Category> level2 = level1.get(i).getChildList();
				if (level2 != null) {
					stringBuffer.append(" ,\"nodes\" : [ ");
					for (int j = 0; j < level2.size(); j++) {
						stringBuffer.append(" { ");
						stringBuffer.append("\"tags\": \""
								+ level2.get(j).getId() + "\"");

						stringBuffer.append(",\"state\": {");
						stringBuffer.append("\"expanded\": true");
						if (selected.equals(level2.get(j).getName())) {
							stringBuffer.append(",\"selected\": true");
						}
						stringBuffer.append("}");

						stringBuffer.append(", \"text\" : \""
								+ level2.get(j).getName() + "\" ");
						stringBuffer.append(" } ");
						if (j < level2.size() - 1) {
							stringBuffer.append(" , ");
						}
					}
					stringBuffer.append(" ] ");
				}
				stringBuffer.append(" } ");
				if (i < level1.size() - 1) {
					stringBuffer.append(" , ");
				}
			}
		}
		stringBuffer.append(" ] }] ");
		return stringBuffer.toString();
	}
}