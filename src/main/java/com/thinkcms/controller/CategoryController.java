package com.thinkcms.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkcms.entity.Article;
import com.thinkcms.entity.Category;
import com.thinkcms.entity.FriendshipLink;
import com.thinkcms.entity.SiteInfo;
import com.thinkcms.entity.SlideImg;
import com.thinkcms.service.ArticleService;
import com.thinkcms.service.CategoryService;
import com.thinkcms.service.FriendshipLinkService;
import com.thinkcms.service.SiteInfoService;
import com.thinkcms.service.SlideImgService;
import com.thinkcms.support.Constants;
import com.thinkcms.support.FtpApche;
import com.thinkcms.support.PageDto;
import com.thinkcms.support.ReqDto;
import com.thinkcms.support.Result;
import com.thinkcms.template.ArticleDirective;
import com.thinkcms.template.FriendLinkDirective;
import com.thinkcms.template.RootChannelDirective;
import com.thinkcms.template.SlideImgDirective;

import freemarker.template.Configuration;
import freemarker.template.Template;


@Controller
@RequestMapping("/category")
public class CategoryController {
	private static Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ArticleService articalService;
	
	@Autowired
	private FriendshipLinkService friendshipLinkService;
	
	@Autowired
	private SiteInfoService siteInfoService;
	
	@Autowired
	private SlideImgService slideImgService;
	
	@RequestMapping(value = "progress", method = {RequestMethod.POST,RequestMethod.GET} )
	@ResponseBody
	public String initCreateInfo(HttpServletRequest request) {
		String message = (String) request.getSession().getAttribute("message");
		if(message==null){
			return "开始生成....";
		}
		return message;
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	@ResponseBody
	public Object uploadHtmlFile(HttpServletRequest request,Model model){
//		String path = System.getProperty("web.root")+Constants.HTML_PATTH;
//		String replayStr = request.getContextPath()+"/"+Constants.HTML_PATTH;
//		LOGGER.info("path的值为"+path+"replayStr的值为"+replayStr);
//		FtpApche ftp = new FtpApche();
//		Boolean result = ftp.uploadFiles(path,path,replayStr,request);
		DetachedCriteria dc = DetachedCriteria.forClass(Category.class);
		dc.add(Restrictions.eq("available", true));
		List<Category> list = categoryService.findList(dc);
		for (Category category : list) {
			try {
				createListHtml(category,request);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
		}
		request.getSession().setAttribute("message", "文件成功上传到服务器");
		return new Result(true, null);
	}

	@RequiresPermissions({ "category/list" })
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String index(Model model) {
		return "category/index";
	}

	@RequiresPermissions({ "category/list" })
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public String list(String name,String listType, ReqDto req, Model model) {
		try {
			model.addAttribute(
					"list",
					categoryService.findList(name,listType, req.getPageNo(),
							req.getPageSize()));
		} catch (Exception e) {
			e.printStackTrace();
			return "common/error";
		}

		return "category/list";
	}

	@RequiresPermissions({ "category/add" })
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		try {
			model.addAttribute("category", getCategoryStr("根节点"));
			model.addAttribute("listTemplate", getTemplateFile("","template/list"));
			model.addAttribute("contentTemplate", getTemplateFile("","template/content"));
			model.addAttribute("suipian", getSuipian("template/public"));
		} catch (Exception e) {
			e.printStackTrace();
			return "common/error";
		}
		return "category/add";
	}

	@RequiresPermissions({ "category/add" })
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Object add(Category Category, Model model) {
		try {
			List<Category> list = categoryService.findByEnName(Category);
			if(list.size()>0){
				return new Result(false, Category);
			}
			if(Category.getParentId() == 0){
				Category.setParentIds(null);
				Category.setHtmlUrl("/"+Category.getEnName());
			}else{
				Category c = categoryService.getCategory(Category.getParentId());
				if(c.getParentIds() != null && !"".equals(c.getParentIds())){
					Category.setParentIds(c.getParentIds()+","+Category.getParentId().toString());
				}else{
					Category.setParentIds(Category.getParentId().toString());
				}
				Category.setHtmlUrl(c.getHtmlUrl()+"/"+Category.getEnName());
			}
			Category = categoryService.saveOrUpdate(Category);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		return new Result(true, Category);
	}

	@RequiresPermissions({ "category/edit" })
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String toEdit(Long id, Model model) {
		try {
			Category Category = categoryService.getCategory(id);
			model.addAttribute("entity", Category);
			model.addAttribute("category",
					getCategoryStr(Category.getParentName()));
			model.addAttribute("listTemplate", getTemplateFile(Category.getListTemplate(),"template/list"));
			model.addAttribute("contentTemplate", getTemplateFile(Category.getContentTemplate(),"template/content"));
			model.addAttribute("suipian", getSuipian("template/public"));
		} catch (Exception e) {
			e.printStackTrace();
			return "common/error";
		}
		return "category/edit";
	}

	@RequiresPermissions({ "category/edit" })
	@ResponseBody
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public Object edit(Category Category, Model model) {
		try {
			if(Category.getParentId() == 0){
				Category.setParentIds(null);
				Category.setHtmlUrl("/"+Category.getEnName());
			}else{
				Category c = categoryService.getCategory(Category.getParentId());
				if(c.getParentIds() != null && !"".equals(c.getParentIds())){
					Category.setParentIds(c.getParentIds()+","+Category.getParentId().toString());
				}else{
					Category.setParentIds(Category.getParentId().toString());
				}
				Category.setHtmlUrl(c.getHtmlUrl()+"/"+Category.getEnName());
			}
			
			Category = categoryService.saveOrUpdate(Category);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		return new Result(true, Category);
	}

	@RequiresPermissions({ "category/del" })
	@ResponseBody
	@RequestMapping(value = "/del", method = RequestMethod.POST)
	public Object del(String ids, Model model) {
		try {
			categoryService.delBatch(ids);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		return new Result();
	}
	
	@ResponseBody
	@RequestMapping(value = "/crateHTML", method = RequestMethod.POST)
	public Object crateHTML(Long id, Model model,HttpServletRequest request) {
		Category Category = categoryService.getCategory(id);
		try {
			createListHtml(Category,request);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, e);
		}
		 //传输静态页面志ftp
        FtpApche ftp = new FtpApche();
        String path = System.getProperty("web.root")+Constants.HTML_PATTH+Category.getHtmlUrl();
        String replayStr = request.getContextPath()+"/"+Constants.HTML_PATTH;
        ftp.uploadOneCategoryFiles(Category.getHtmlUrl(), path, replayStr,request);
		return new Result();
	}	
	
	@RequestMapping(value = "/allHtml",method=RequestMethod.POST)
	@ResponseBody
	public Object allHtml(HttpServletRequest request){
		DetachedCriteria dc = DetachedCriteria.forClass(Category.class);
		dc.add(Restrictions.eq("available", true));
		List<Category> list = categoryService.findList(dc);
		for (Category category : list) {
			try {
				createListHtml(category,request);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String path = System.getProperty("web.root")+Constants.HTML_PATTH;
		String replayStr = request.getContextPath()+"/"+Constants.HTML_PATTH;
		LOGGER.info("path的值为"+path+"replayStr的值为"+replayStr);
		FtpApche ftp = new FtpApche();
		Boolean result = ftp.uploadFiles(path,path,replayStr,request);
		request.getSession().setAttribute("message", "文件成功上传到服务器");
		return new Result(result, null);
	}
	/**
	 * 生成静态页面
	 * @param Category
	 * @return
	 * @throws IOException 
	 */
	private void createListHtml(Category Category,HttpServletRequest request) throws Exception{
		DetachedCriteria dc = DetachedCriteria.forClass(Article.class);
		dc.add(Restrictions.eq("available", true));
		dc.add(Restrictions.eq("categoryId", Category.getId()));
		dc.addOrder(Order.desc("showOrder"));
		PageDto<Article> articallist = articalService.findList(dc, 1, Constants.PAGE_SIZE);
		List<Article> getArticalByCategory = articalService.findByParams(dc);
		int totalpage = 0;
		if(articallist.getTotalCount()%articallist.getPageSize() == 0){
			totalpage = (int) (articallist.getTotalCount()/articallist.getPageSize());
		}else{
			totalpage = (int) (articallist.getTotalCount()/articallist.getPageSize()) + 1;
		}
		Map<String,Object> data = Maps.newHashMap();
		
		 //设置当前栏目的子栏目
        long id = Category.getId();
        DetachedCriteria cd = DetachedCriteria.forClass(Category.class);
        cd.add(Restrictions.eq("parentId", id));
        cd.add(Restrictions.eq("available", true));
        cd.addOrder(Order.desc("showOrder"));
        //当前栏目所有子栏目集合
    	List<Category> childrenCategory = categoryService.findList(cd);
    	if(childrenCategory != null && childrenCategory.size()>0){
    		Category.setChildList(childrenCategory);
    	}
		
		//获取当前栏目信息
        data.put("category", Category);
        data.put("request", request);
       
        
        String parentIds = Category.getParentIds();
        if(parentIds != null && !"".equals(parentIds)){
        	DetachedCriteria d = DetachedCriteria.forClass(Category.class);
        	String[] ids = parentIds.split(",");
        	List<Long> lids = Lists.newArrayList();
        	for (String s : ids) {
        		lids.add(Long.parseLong(s));
			}
        	d.add(Restrictions.in("id", lids));
        	d.add(Restrictions.eq("available", true));
        	d.addOrder(Order.desc("showOrder"));
        	//当前栏目所有父目录集合
        	List<Category> parentCategory = categoryService.findList(d);
        	
        	//当前栏目的父栏目
        	Category pCategory = categoryService.getCategory(Category.getParentId());
        	
        	DetachedCriteria bd = DetachedCriteria.forClass(Category.class);
        	bd.add(Restrictions.eq("parentId", Category.getParentId()));
        	bd.add(Restrictions.eq("available", true));
        	bd.addOrder(Order.desc("showOrder"));
        	//当前栏目所有兄弟目录集合
        	List<Category> brotherCategory = categoryService.findList(bd);
        	
        	data.put("pCategory", pCategory);
        	data.put("parentCategory", parentCategory);
        	data.put("brotherCategory", brotherCategory);
        }
        //站点信息数据
  		SiteInfo siteinfo = siteInfoService.getSiteInfo((long)1);
  		data.put("siteinfo", siteinfo);
  		
  		//获取自定义文章标签数据
  		data.put("myarticle", new ArticleDirective(articalService,categoryService));
  		
  		//获取友情链接标签数据
  		data.put("friendLink", new FriendLinkDirective(friendshipLinkService));
  		
  		//获取banner切换图片数据
  		data.put("slideImg", new SlideImgDirective(slideImgService));
  		
  		//获取根栏目数据
  		data.put("rootChannel", new RootChannelDirective(categoryService));
  		
        //生成列表页静态页面
        String listhtmlpath = getHtmlPath(Category,1);
        if(totalpage>=1){
	        for(int i=1;i<totalpage+1;i++){
	        	PageDto<Article> art = articalService.findList(dc, i, Constants.PAGE_SIZE);
	        	for (Article  a : art.getItems()) {
					String c = StringEscapeUtils.unescapeJava(a.getContent());
					a.setContent(c);
				}
	        	Integer totalPage = (int) (art.getTotalCount() % art.getPageSize() > 0 ? (art.getTotalCount()
	    				/ art.getPageSize() + 1) : (art.getTotalCount() / art.getPageSize()));
	    		Integer prePage = art.getPageNo() - 1 > 0 ? art.getPageNo() - 1 : 1;
	    		Integer nextPage = art.getPageNo() + 1 > totalPage ? totalPage : art.getPageNo() + 1;
	    		data.put("totalPage", totalPage);
	    		data.put("prePage", prePage);
	    		data.put("nextPage", nextPage);
	    		data.put("artpage", art);
	        	if(i==1){
	        		crateHTML(getConfiguration(),data,Constants.LIST_TEMPLATE+Category.getListTemplate(),listhtmlpath+"/index.html");
	        	}else{
	        		crateHTML(getConfiguration(),data,Constants.LIST_TEMPLATE+Category.getListTemplate(),listhtmlpath+"/index_"+i+".html");
	        	}
	        	data.remove("artpage");
	        	data.remove("totalPage");
	        	data.remove("prePage");
	        	data.remove("nextPage");
	        }
        }else{
        	crateHTML(getConfiguration(),data,Constants.LIST_TEMPLATE+Category.getListTemplate(),listhtmlpath+"/index.html");
        }
        
        //生成内容页静态页面
        if(Category.getContentTemplate() != null && !"".equals(Category.getContentTemplate())){
	        String contenthtmlpath = getHtmlPath(Category,2);
	        for (Article article : getArticalByCategory) {
	        	String c = StringEscapeUtils.unescapeJava(article.getContent());
	        	article.setContent(c);
	        	data.put("art", article);
	        	crateHTML(getConfiguration(),data,Constants.CONTENT_TEMPLATE+Category.getContentTemplate(),contenthtmlpath+"/"+article.getId()+".html");
	        	data.remove("art");
	        }
        }
	}
	
	/**
	 * 获得Configuration对象
	 * @param request 
	 * @param listPath
	 * @return
	 * @throws IOException
	 */
	private Configuration getConfiguration() throws IOException{
		Configuration conf = new Configuration();
//		String dir = this.getClass().getClassLoader().getResource(Constants.BASE_TEMPLATE).getPath();
		String dir = System.getProperty("web.root");
		conf.setDirectoryForTemplateLoading(new File(dir));
		return conf;
	}
	
	/**
	 * 获得页面最终存放路径
	 * @param category
	 * @param type   1.文件夹   2.文件
	 * @return
	 */
	private String getHtmlPath(Category category,int type){
        String webappRoot = System.getProperty("web.root");
        String htmlPath = webappRoot+Constants.HTML_PATTH+category.getHtmlUrl();
        File f = new File(htmlPath);
        if(!f.exists()){
        	f.mkdirs();
        }else{
        	File []files = f.listFiles();
        	for (File file : files) {
				if(type == 1){
					if(file.getName().contains("index")){
						file.delete();
					}
				}else if(type == 3){
					if(file.getName().contains("sp")){
						file.delete();
					}
				}else{
					if(!file.getName().contains("index") && !file.getName().contains("sp")){
						file.delete();
					}
				}
			}
        }
        System.out.println("最终存放地址为--"+htmlPath+"类型为--->"+type);
		return htmlPath;
	}
	
	/**
	 * 
	 * @param freemarkerCfg	模板对象
	 * @param data					页面所需数据对象
	 * @param templatePath		模板路径
	 * @param targetHtmlPath	静态页面路径
	 */
    public  void crateHTML(Configuration freemarkerCfg,Map<String,Object> data,String templatePath,String htmlPath) throws Exception{
    	Template template = freemarkerCfg.getTemplate(templatePath,"UTF-8");
        File htmlFile = new File(htmlPath);
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(htmlFile), "UTF-8"));
        template.process(data, out);
        out.flush();
        out.close();
    }

	private ArrayList<String>  getSuipian(String url){
		String path = System.getProperty("web.root")+url;
		System.out.println("path的内容为---"+path);
		ArrayList<String> listFileName = new ArrayList<String>(); 
        getAllFileName(path,listFileName);
        return listFileName ;
	}
	
	private String getTemplateFile(String templateUrl,String url){
		String path =  System.getProperty("web.root")+url;
        ArrayList<String> listFileName = new ArrayList<String>(); 
        getAllFileName(path,listFileName);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[ ");
        for(String name:listFileName)
        {
            stringBuffer.append("{\"text\": \"" + name + "\",");
    		stringBuffer.append("\"tags\": \"" + name + "\"");
    		stringBuffer.append(",\"state\": {");
    		stringBuffer.append("\"expanded\": false");
    		if(templateUrl != null && !"".equals(templateUrl)){
	    		if (templateUrl.equals(name)) {
	    			stringBuffer.append(",\"selected\": true");
	    		}
    		}
    		stringBuffer.append("}},");
        }
        String s =stringBuffer.substring(0, stringBuffer.length()-1);
        s = s+"]";
        return s;
	}
	
	private String getCategoryStr(String selected) {
		Category category = categoryService.getRoot();
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[ {");
		stringBuffer.append("\"text\": \"" + category.getName() + "\",");
		stringBuffer.append("\"tags\": \"" + category.getId() + "\"");
		stringBuffer.append(",\"state\": {");
		stringBuffer.append("\"expanded\": false");
		if (selected.equals(category.getName())) {
			stringBuffer.append(",\"selected\": true");
		}
		stringBuffer.append("}");
		stringBuffer.append(",\"nodes\" : [ ");
		List<Category> level1 = category.getChildList();
		if (level1 != null) {
			for (int i = 0; i < level1.size(); i++) {
				stringBuffer.append(" { ");
				stringBuffer.append("\"tags\": \"" + level1.get(i).getId()
						+ "\",");
				stringBuffer.append(" \"text\" : \"" + level1.get(i).getName()
						+ "\" ");

				stringBuffer.append(",\"state\": {");
				stringBuffer.append("\"expanded\": true");
				if (selected.equals(level1.get(i).getName())) {
					stringBuffer.append(",\"selected\": true");
				}
				stringBuffer.append("}");

				List<Category> level2 = level1.get(i).getChildList();
				if (level2 != null) {
					stringBuffer.append(" ,\"nodes\" : [ ");
					for (int j = 0; j < level2.size(); j++) {
						stringBuffer.append(" { ");
						stringBuffer.append("\"tags\": \""
								+ level2.get(j).getId() + "\"");

						stringBuffer.append(",\"state\": {");
						stringBuffer.append("\"expanded\": true");
						if (selected.equals(level2.get(j).getName())) {
							stringBuffer.append(",\"selected\": true");
						}
						stringBuffer.append("}");

						stringBuffer.append(", \"text\" : \""
								+ level2.get(j).getName() + "\" ");
						stringBuffer.append(" } ");
						if (j < level2.size() - 1) {
							stringBuffer.append(" , ");
						}
					}
					stringBuffer.append(" ] ");
				}
				stringBuffer.append(" } ");
				if (i < level1.size() - 1) {
					stringBuffer.append(" , ");
				}
			}
		}
		stringBuffer.append(" ] }] ");
		return stringBuffer.toString();
	}
	
    public  void getAllFileName(String path,ArrayList<String> fileName)
    {
        File file = new File(path);
        File [] files = file.listFiles();
        String [] names = file.list();
        if(names != null)
        fileName.addAll(Arrays.asList(names));
        for(File a:files)
        {
            if(a.isDirectory())
            {
                getAllFileName(a.getAbsolutePath(),fileName);
            }
        }
    }
    
    
    
    
}