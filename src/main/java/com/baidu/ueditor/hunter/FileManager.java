package com.baidu.ueditor.hunter;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.baidu.ueditor.PathFormat;
import com.baidu.ueditor.define.AppInfo;
import com.baidu.ueditor.define.BaseState;
import com.baidu.ueditor.define.MultiState;
import com.baidu.ueditor.define.State;
import com.thinkcms.entity.Attachment;
import com.thinkcms.service.AttachmentService;
import com.thinkcms.support.Constants;
import com.thinkcms.support.PageDto;

public class FileManager {

	private String dir = null;
	private String rootPath = null;
	private String[] allowFiles = null;
	private int count = 0;

	public FileManager ( Map<String, Object> conf ) {

		this.rootPath = (String)conf.get( "rootPath" );
		this.dir = this.rootPath + (String)conf.get( "dir" );
		this.allowFiles = this.getAllowFiles( conf.get("allowFiles") );
		this.count = (Integer)conf.get( "count" );
		
	}
	
	public State listFile ( int index ) {
		State state = null;
		WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();
		ServletContext servletContext = webApplicationContext.getServletContext();
		ApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		AttachmentService attachmentService  = (AttachmentService) ctx.getBean("attachmentService");
		if(attachmentService != null){
			PageDto<Attachment> page = attachmentService.findList(null, null, "image", index, 10000);
			state = this.getNewState(page.getItems().toArray());
			state.putInfo( "start", index );
			state.putInfo( "total", page.getTotalCount() );
		}else{
			return new BaseState( false, AppInfo.NOT_EXIST );
		}
		
//		File dir = new File( this.dir );
//		
//
//		if ( !dir.exists() ) {
//			return new BaseState( false, AppInfo.NOT_EXIST );
//		}
//		
//		if ( !dir.isDirectory() ) {
//			return new BaseState( false, AppInfo.NOT_DIRECTORY );
//		}
//		
//		Collection<File> list = FileUtils.listFiles( dir, this.allowFiles, true );
//		
//		if ( index < 0 || index > list.size() ) {
//			state = new MultiState( true );
//		} else {
//			Object[] fileList = Arrays.copyOfRange( list.toArray(), index, index + this.count );
//			state = this.getState( fileList );
//		}
//		
//		state.putInfo( "start", index );
//		state.putInfo( "total", list.size() );
		
		return state;
		
	}
	
	private State getNewState(Object[] files){
		MultiState state = new MultiState( true );
		BaseState fileState = null;
		for (Object obj : files) {
			if ( obj == null ) {
				break;
			}
			Attachment att = (Attachment)obj;
		   fileState = new BaseState( true );
		   fileState.putInfo( "url", PathFormat.format( att.getUrl() ) );
		   state.addState( fileState );
		}
		return state;
	}
	
	private State getState ( Object[] files ) {
		
		MultiState state = new MultiState( true );
		BaseState fileState = null;
		
		File file = null;
		
		for ( Object obj : files ) {
			if ( obj == null ) {
				break;
			}
			file = (File)obj;
			fileState = new BaseState( true );
			fileState.putInfo( "url", PathFormat.format( this.getPath( file ) ) );
			state.addState( fileState );
		}
		
		return state;
		
	}
	
	private String getPath ( File file ) {
		
		String path = file.getAbsolutePath();
		
		return path.replace( this.rootPath, "/" );
		
	}
	
	private String[] getAllowFiles ( Object fileExt ) {
		
		String[] exts = null;
		String ext = null;
		
		if ( fileExt == null ) {
			return new String[ 0 ];
		}
		
		exts = (String[])fileExt;
		
		for ( int i = 0, len = exts.length; i < len; i++ ) {
			
			ext = exts[ i ];
			exts[ i ] = ext.replace( ".", "" );
			
		}
		
		return exts;
		
	}
	
}
