package thinkcms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.google.common.collect.Maps;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class Test {
	public static void main(String[] args) throws IOException {
//		// 配置对象 .html
		Configuration conf = new Configuration();
//		// 模板路径
        String dir = Test2.class.getClassLoader().getResource("template").getPath();;
        try {
            // 导入模板目录
            conf.setDirectoryForTemplateLoading(new File(dir));
            // 获取模板
            Template template = conf.getTemplate("index.ftl");
            // 数据
            Map root = Maps.newHashMap();
            root.put("world", "世界");
            // 输出流
            Writer out = new FileWriter(new File(dir+"/" + "hello.html"));
            // 生成开始
            template.process(root, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("生成完毕");
	}
}
